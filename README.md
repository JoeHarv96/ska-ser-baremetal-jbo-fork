# SKA Openstack Infra

The SKA Openstack Infra is a repository used to manage the baremetal infrastructure supervised by SKAO. It uses [Terraform](https://www.terraform.io/) for orchestration and [Ansible](https://www.ansible.com/) for installation/configuration.

# Prerequisites

It does not have any direct dependencies but check the READMEs of each submodule for an updated list of requirements:

* [SKA Orchestration](./ska-ser-orchestration/README.md#prerequisites)
* [SKA Makefile](./.make/README.md)
* [OpenStack Kayobe](./kayobe/README.rst)
* [OpenStack Tenks](./tenks/README.rst)

The SKA Orchestration project requirements can be installed using the following make target, which will install the dependencies in a virtual environment and open a poetry shell:

```bash
make requirements
```

Terraform also needs to be installed in the system, please follow a recent guide to do so.

## Environment Variables

Like the submodules for Terraform and Ansible, this repository does not have any default variables when running the Makefile targets to avoid any deployment/installation on the wrong cluster my mistake.

So, the first variables to setup are the **DATACENTRE**, **ENVIRONMENT**, and **SERVICE**. Like the name suggest, they point to the datacentre, environment and service we want to work with. These map to the folder structure under [environments](environments/) (`environments/<datacentre>/<environment>/<service>`), which contains the orchestration and installation files.

For doing that please add a PrivateRules.mak with the following variables:

```bash
DATACENTRE="<DATACENTRE>"
ENVIRONMENT="<environment>"
SERVICE="<service>"
TF_HTTP_USERNAME="<gitlab-username>" # Gitlab User token with the API scope
TF_HTTP_PASSWORD="<user-token>"
```

Follow this [link](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) to create the Gitlab User token with the API scope. If this variable is empty, the Makefile targets will not run for security reasons.

### Other important variables

For Terraform the following specifics variables for the Gitlab's backend are already set in the Makefile:

```bash
BASE_PATH?="$(shell cd "$(dirname "$1")"; pwd -P)"
GITLAB_PROJECT_ID?=39999641
TF_ROOT_DIR?=${BASE_PATH}/environments/${DATACENTRE}/${ENVIRONMENT}/${SERVICE}/orchestration
TF_HTTP_ADDRESS?=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${DATACENTRE}-${ENVIRONMENT}-${SERVICE}-terraform-state
TF_HTTP_LOCK_ADDRESS?=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${DATACENTRE}-${ENVIRONMENT}-${SERVICE}-terraform-state/lock
TF_HTTP_UNLOCK_ADDRESS?=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${DATACENTRE}-${ENVIRONMENT}-${SERVICE}-terraform-state/lock
PLAYBOOKS_ROOT_DIR?=${BASE_PATH}/environments/${DATACENTRE}/${ENVIRONMENT}/${SERVICE}/installation
ANSIBLE_CONFIG?=${BASE_PATH}/environments/${DATACENTRE}/${ENVIRONMENT}/${SERVICE}/installation/ansible.cfg
ANSIBLE_COLLECTIONS_PATHS?=${BASE_PATH}/ska-ser-ansible-collections
```

The PLAYBOOKS_ROOT_DIR indicates where the inventory file and the respective group variables are.
Change them carefully if you really need it.

The Makefile has a help target to print these variables and all available targets:

```bash
make help
```

# How to use

## Make Targets

The Makefile available in this repository has two main targets which redirect to the two different functionalities that this repository provide.

For [Terraform make targets](./ska-ser-orchestration/Makefile) we must use **orch** as first argument and **playbooks** , for [Ansible Targets](./ska-ser-ansible-collections/Makefile).
All the shell env variables are saved and the command arguments are carried over.

Always check the READMEs for [orchestration](./ska-ser-orchestration/README.md#Getting&#32;started)
and [installation](./ska-ser-ansible-collections/README.md#Usage) for up-to-date setup and how to use recommendations.

## Project Structure

 This a single repository that can manage multiple datacentres, environments and services, so the first step is
 to select which one we want. Inside the **./environments/** folder, we have all the configurations and variables separated by datacentre (cluster), environment and service.

| Datacentre        | Environment   | Folder Path                                    |
| ----------------- | ------------- | ---------------------------------------------- |
| techops           | test          | ./datacentres/techops/environments/test        |
| techops           | integration   | ./datacentres/techops/environments/integration |
| engage            | sandbox       | ./datacentres/engage/environments/sandbox      |

Inside each environment subdirectory, we divide the config files for Terraform (orchestration)
and Ansible (installation). Like the example bellow:

```text
.
├── Makefile
├── datacentres
|   ├── <datacentre>
|   |   ├──<environment>
│   │   │   ├── installation
│   │   │   │   │   ├── ansible.cfg
│   │   │   │   │   ├── group_vars
│   │   │   │   │   │   └── *.yml
│   │   │   │   │   ├── inventory.yml
│   │   │   │   │   ├── playbooks
│   │   │   │   │   │   └── *.yml
│   │   │   │   │   └── ssh.config
│   │   │   ├── orchestration
|   |   |   |   ├── <service>
│   │   │   │   │   ├── *.tf
│   │   │   │   │   ├── clouds.yaml
│   │   │   │   │   └── terraform.tfvars
|   |   └── ...
|   └── ...
├── ska-ser-ansible-collections
└── ska-ser-orchestration.
```

To add a new datacentre/environment/service, all that is required is to create the appropriate folder structure under [environments](environments/), add the necessary files there for orchestration and installation, and to ensure that the required variables are set in the PrivateRules.mak file as previously explained.

## Modifying the Project Structure

If the folder structure needs to be altered in some way, e.g. if a service or datacentre must be renamed, then the state file needs to be updated since its name directly references the folder structure.

After the folder structure is updated, you must run the following commands for all services affected by the migration (by updating the PrivateRules.mak between each execution):

```bash
make orch init
make orch refresh
```

## Orchestration on Openstack

Any Terraform files (*.tf) inside the orchestration folder will be analysed and applied. So every service/VM is described there and use the modules on the **ska-ser-orchestration** submodule.

The **clouds.yaml** file should also be on this folder alongside the Terraform files.
This is the only supported authentication for Openstack API. Go to the Openstack
Web interface and created a new credential on *Identity > Application Credentials*
page. **If you see an error, you may need to create an instance and wait a couple of hours if you haven't done so previously.**

Finally, you just have to init Terraform locally and apply the changes:

```bash
make orch init
make orch apply
```

Recommendation: Setup the **TF_TARGET** to only apply/destroy to a specific service/VM. The module name should be name on the first line of the module definition:

```bash
make orch init
make orch apply TF_TARGET="module.<your-module-name>"
make orch destroy TF_TARGET="module.<your-module-name>"
```

## Installation via Ansible

The **ssh.config** and **inventory.yml** files automatically generated using:

```bash
make orch generate-inventory
```

This target call a script to retrieve the TF state from Gitlab and compiles the
data to generate those two files and automatically moves them to the **$PLAYBOOKS_ROOT_DIR**.

Finally, run the installation make targets of your choosing.

## Deploy Seed

Creating the node with terraform and updated the inventory.

```bash
make orch init
make orch apply TF_TARGET="module.openstack-instance"
make orch generate-inventory
```

Make sure the inventory files are in the kayobe installation inventory folder
You need to set in your PrivateRules file the PLAYBOOKS_HOSTS=node-name

```bash
make seed-from-nothing
```

This will deploy the aufn until you have a seed host with bifrost running.

### Configure S3 Bucket Credentials

As part of the seed deployment, a periodic seed database backup process is also configured.
This process makes use of AWS S3 buckets to store the backups off-site, meaning that the authentication credentials for the bucket must be defined.

To do this, the following variables must be defined in the PrivateRules.mak file:

```bash
AWS_S3_ACCESS_KEY = ...
AWS_S3_SECRET_KEY = ...
SEED_BACKUP_SLACK_WEBHOOK = ...
```

In addition, the following variables must be defined to ensure the automatic backup process doesn't start while a power failure event is ongoing.
If there is no UPS, they should be left empty.

```bash
SNMP_UPS_AUTH_COMMUNITY = ...
SNMP_UPS_VERSION = [1|2|3]
SNMP_UPS_IP = ...
```

## Deploy the hypervisor for the infravm

With the seed node created you can also create a second node that can be configured with a hypervisor and a infravm on to of it. Creating the new node with terraform and updated the inventory.

```bash
make orch init
make orch apply TF_TARGET="module.infravm-instance"
make orch generate-inventory
```

Make sure the inventory files are in the kayobe installation inventory folder
You need to set in your PrivateRules file the PLAYBOOKS_HOSTS=<< node-name >> and the INFRAVM_HYPERVISOR_HOST=<< node-name >>
You also need to pass the values the new infravm will use in the /kayobe/inventory/group_vars/all.yml. Example:

```yaml
infravms_hypervisor_ip: "192.168.33.x"

infravms:
  - name: "infravm1"
    host_ip: "192.168.33.x"
    memory_gb: "6"
  - name: "infravm2"
    host_ip: "192.168.33.x"
    memory_gb: "4"
```

Then just run the following targets:

```bash
make infravm-setup-inventory

make infravm-create
```

This will generate a inventory that will be placed in the seed node.
It will also configure the this second node with network and credentials
Then configures the hypervisor and deploys a infravm on it.

## Customizable service deploy

In a way to manage witch services are deploy in each environments, we can deploy custom playbooks that can group together playbooks/roles.

Tu use it place the playbooks inside a **playbooks** folder in the datacentre->environment instalation folder (see the section "Project Structure" above) and make sure **DATACENTRE** and **ENVIRONMENT** variables are defined.

To call this custom playook call the target as follow:

`make service [name_of_the_playbook_file]`

example:

`make service gateway`

## Prometheus Openstack Exporter

To ensure that the monitoring services deployed via the ska-ser-ansible-collections scrape the openstack metrics exposed by the Prometheus OpenStack Exporter, the following ansible variable must be defined that points to the Kayobe monitor node:

```yaml
prometheus_openstack_exporter_addr: "<monitor-node-ip>:9198"
```
