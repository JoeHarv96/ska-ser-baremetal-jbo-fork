#!/bin/sh



# Read JSON data from a the inventory files that contain ip user and password
json_data=$(cat inventory.json)

# Get the length of the array
array_length=$(echo "$json_data" | jq '. | length')

for ((i=0; i<$array_length; i++)); do
  IP=$(echo "$json_data" | jq -r ".[$i].IP")
  USERNAME=$(echo "$json_data" | jq -r ".[$i].USER")
  PASS=$(echo "$json_data" | jq -r ".[$i].PASSWORD")

  echo "IP: $IP"
  info=$(python3 python/GetStorageInventoryREDFISH.py -ip ${IP} --ssl false -u ${USERNAME} -p "${PASS}" --get-disks | sed "s/'/\"/g" | sed 's/ False/ "False"/g' | sed 's/ None/ "None"/g' | sed 's/ True/ "True"/g' | jq -r '.[] | ."CapacityBytes",."MediaType",."Id"') 
  python3 python/GetDiskOperationREDFISH.py -ip ${IP} --ssl false -u ${USERNAME} -p "${PASS}" --get-controllers

  for infoss in ${info[@]};do

  re='^[0-9]+$'

  if [[ $infoss =~ $re ]] ; then
  infoss=$(numfmt --to iec --format "%8.4f" $infoss)
  fi
  echo $infoss
  done
    
done
