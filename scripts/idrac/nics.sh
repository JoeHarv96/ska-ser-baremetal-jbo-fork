#!/bin/sh


# Read JSON data from a the inventory files that contain ip user and password
json_data=$(cat inventory.json)

# Get the length of the array
array_length=$(echo "$json_data" | jq '. | length')

for ((i=0; i<$array_length; i++)); do
  IP=$(echo "$json_data" | jq -r ".[$i].IP")
  USERNAME=$(echo "$json_data" | jq -r ".[$i].USER")
  PASS=$(echo "$json_data" | jq -r ".[$i].PASSWORD")

  echo "IP: $IP"
	
  nics=$(python3 python/GetEthernetInterfacesREDFISH.py -ip ${IP} --ssl false -u ${USERNAME} -p "${PASS}" --get-all | sed "s/'/\"/g" | sed 's/ False/ "False"/g' | sed 's/ None/ "None"/g' | sed 's/ True/ "True"/g')

	nic_info=$(echo "$nics" | jq '.[] | {LinkStatus: .LinkStatus, Id: .Id, MACAddress: .MACAddress}')
	nic_id=$(echo "$nics" | jq '.[] | {Id: .Id}')

	echo "$nic_id" | jq -c '.Id' | while read -r id; do
  id="${id%\"}"
  id="${id#\"}"
  python3 python/SetNetworkDevicePropertiesREDFISH.py -ip ${IP} --ssl false -u ${USERNAME} -p "${PASS}" --get-port-details $id
  done
  
done
