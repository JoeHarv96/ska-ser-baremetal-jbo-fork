#!/bin/sh

# Read JSON data from a the inventory files that contain ip user and password
json_data=$(cat inventory.json)

# Get the length of the array
array_length=$(echo "$json_data" | jq '. | length')

for ((i=0; i<$array_length; i++)); do
  IP=$(echo "$json_data" | jq -r ".[$i].IP")
  USERNAME=$(echo "$json_data" | jq -r ".[$i].USER")
  PASS=$(echo "$json_data" | jq -r ".[$i].PASSWORD")

  echo "IP: $IP"

  python3 python/GetSetBiosAttributesREDFISH.py -ip ${IP} --ssl false -u ${USERNAME} -p "${PASS}" --get-attribute SystemServiceTag

  python3 python/GetSetBiosAttributesREDFISH.py -ip ${IP} --ssl false -u ${USERNAME} -p "${PASS}" --get-attribute SystemModelName

done


