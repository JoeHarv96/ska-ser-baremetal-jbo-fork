# iDrac Redfish Scripts

Python and PowerShell scripting for Dell EMC PowerEdge iDRAC REST API with DMTF Redfish.

Main repo: https://github.com/dell/iDRAC-Redfish-Scripting

This scripts can be used to configure iDRAC and get the iDRAC and server information without the need of using the iDRAC interface.


## How to use

To use these scripts you will need to setup the inventory with the IP, User and Password for each iDRAC that you want to communicate with. For that you can use the `inventory.json.sample` file and copy it to a file named `inventory.json` in the same folder. This process is used so we can gitignore it and mitigate the risk of exposing iDRAC credentials.

After having the `inventory.json` file configured you can use any of the scripts present on this folder.

## Scripts

### NICS

The nics.sh script will do a call to the iDRACs and output the following fields for all the nics of that iDRAC server:

* **LinkStatus:** This gives us the information about the status of the link. (ie. LinkDown, LinkUp or None)
* **Id:**: This field retrieves the id value that can be used to configure the bios network interface for example. (i.e NIC.Slot.6-1-1)
* **MACAddress**: This field contains the MAC address of each nic, and can be very useful to help us identify to which interface on the server this nic is connected to.

### Verify Disk

The verify_disks.sh script retrieves a list of specifications relating the physical disks present on the iDRAC server, the script can be very useful to help building the raid config files.

This script retrieves:

* **Controllers:** Disk Controllers of the iDRAC server 
* **Size of the disk:** It retrieves the size of the physical disk (i.e 3.4926T)
* **Type of disk:** The type of the disk (i.e SSD or HDD)
* **Disk ID:** The disk ID (i.e Disk.Bay.0:Enclosure.Internal.0-1:RAID.SL.8-1)

### Bios Config

This script will configure the Bios Interface configuration, more specifically PxeDev1Interface with the given attribute value. This is done in parallel for all the iDRACs as this script can take some time because it needs to **reboot** the iDRAC to apply the changes.

### Inspection Info

This script returns the values of **SystemModelName** and **SystemServiceTag** attributes that can be used in the introspection rules to identify the right baremetal servers. As the **SystemServiceTag** is unique for each Server and some servers can be grouped with the **SystemModelName** attribute.