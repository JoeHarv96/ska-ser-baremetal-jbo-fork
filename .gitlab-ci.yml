image: $SKA_K8S_TOOLS_BUILD_DEPLOY

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  DATACENTRE: "stfc-techops"
  ENVIRONMENT: "test"
  SERVICE: "aufn"
  GITLAB_PROJECT_ID: $CI_PROJECT_ID
  BASE_PATH: $CI_PROJECT_DIR
  ENVIRONMENT_ROOT_DIR: $BASE_PATH/datacentres/$DATACENTRE/$ENVIRONMENT
  TF_ROOT_DIR: $ENVIRONMENT_ROOT_DIR/orchestration/$SERVICE
  POETRY_HOME: "/opt/poetry"
  TERRAFORM_VERSION: "1.3.7"
  TF_HTTP_USERNAME: $GITLAB_API_REQUESTER
  TF_HTTP_PASSWORD: $GITLAB_API_PRIVATE_TOKEN
  PLAYBOOKS_ROOT_DIR: $ENVIRONMENT_ROOT_DIR/installation
  TF_INVENTORY_DIR: $PLAYBOOKS_ROOT_DIR
  INVENTORY: $PLAYBOOKS_ROOT_DIR
  TF_SUFFIX: "-$CI_COMMIT_SHORT_SHA"
  TF_VAR_instname: ci-$CI_PROJECT_ID-$DATACENTRE-$ENVIRONMENT-$SERVICE$TF_SUFFIX
  TF_VAR_hypervisor_instname: ci-$CI_PROJECT_ID-$DATACENTRE-$ENVIRONMENT-$SERVICE-hypervisor$TF_SUFFIX
  ANSIBLE_CONFIG: "$PLAYBOOKS_ROOT_DIR/ansible.cfg"
  ANSIBLE_SSH_ARGS: "-o ControlPersist=30m -o ControlMaster=auto -o StrictHostKeyChecking=no -F $PLAYBOOKS_ROOT_DIR/ssh.config"
  ANSIBLE_SSH_RETRIES: 5
  ANSIBLE_SSH_TIMEOUT: 60
  TF_HTTP_ADDRESS: https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/terraform/state/$DATACENTRE-$ENVIRONMENT-$SERVICE-terraform-state$TF_SUFFIX
  TF_HTTP_LOCK_ADDRESS: $TF_HTTP_ADDRESS/lock
  TF_HTTP_UNLOCK_ADDRESS: $TF_HTTP_ADDRESS/lock

stages:
   - lint
   - deploy
   - baremetal
   - test
   - destroy

create_seed:
  stage: deploy
  image: python:3.9
  tags:
    - k8srunner
  allow_failure: false
  script:
    - pip3 install ansible requests poetry openstackclient
    - wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -O terraform.zip
    - unzip terraform.zip && rm terraform.zip && mv terraform /usr/bin/terraform
    - poetry install
    - cp $CLOUDS_YAML $TF_ROOT_DIR/clouds.yaml
    - make orch init SECURITY_CHECK=OFF
    - make orch apply SECURITY_CHECK=OFF TF_ARGUMENTS="-auto-approve"
    - make orch generate-inventory SECURITY_CHECK=OFF TF_ARGUMENTS="-auto-approve"
  artifacts:
    paths:
      - $PLAYBOOKS_ROOT_DIR
    expire_in: 1 day
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_JOB == "kayobe-run"

kayobe:
  stage: baremetal
  image: python:3.9
  tags:
    - k8srunner
  allow_failure: false
  script:
    - pip3 install ansible requests
    - cp $TECHOPS_KEY $CI_PROJECT_DIR/ska-techops.pem
    - chmod 600 $CI_PROJECT_DIR/ska-techops.pem
    - make seed-from-nothing SECURITY_CHECK=OFF PLAYBOOKS_HOSTS=$TF_VAR_instname SKIP_TAGS=setup-seed-backup
  dependencies:
    - create_seed
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_JOB == "kayobe-run"

smoke-test:
  needs: ["create_seed","kayobe"]
  stage: baremetal
  image: python:3.9
  tags:
    - k8srunner
  allow_failure: false
  script:
    - pip3 install ansible requests
    - cp $TECHOPS_KEY $CI_PROJECT_DIR/ska-techops.pem
    - chmod 600 $CI_PROJECT_DIR/ska-techops.pem
    - make smoke-test-ironic SECURITY_CHECK=OFF PLAYBOOKS_HOSTS=$TF_VAR_instname
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_JOB == "kayobe-run"

tenks:
  stage: test
  image: python:3.9
  tags:
    - k8srunner
  allow_failure: false
  script:
    - pip3 install ansible requests
    - cp $TECHOPS_KEY $CI_PROJECT_DIR/ska-techops.pem
    - chmod 600 $CI_PROJECT_DIR/ska-techops.pem
    - make tenks-ironic-test SECURITY_CHECK=OFF PLAYBOOKS_HOSTS=$TF_VAR_instname
  dependencies:
    - create_seed
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_JOB == "kayobe-run"

openstack:
  needs: ["create_seed","tenks"]
  stage: test
  image: python:3.9
  tags:
    - k8srunner
  allow_failure: false
  script:
    - pip3 install ansible requests
    - cp $TECHOPS_KEY $CI_PROJECT_DIR/ska-techops.pem
    - chmod 600 $CI_PROJECT_DIR/ska-techops.pem
    - make test-deploy-openstack SECURITY_CHECK=OFF PLAYBOOKS_HOSTS=$TF_VAR_instname
  artifacts:
    paths:
      - reports
    expire_in: 1 month
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_JOB == "kayobe-run"

infravm-setup-inventory:
  needs: ["kayobe"]
  stage: baremetal
  image: python:3.9
  tags:
    - k8srunner
  allow_failure: false
  script:
    - pip3 install ansible requests
    - cp $TECHOPS_KEY $CI_PROJECT_DIR/ska-techops.pem
    - chmod 600 $CI_PROJECT_DIR/ska-techops.pem
    - make orch generate-inventory SECURITY_CHECK=OFF TF_ARGUMENTS="-auto-approve"
    - make infravm-setup-inventory SECURITY_CHECK=OFF PLAYBOOKS_HOSTS=$TF_VAR_instname INFRAVM_HYPERVISOR_HOST=$TF_VAR_hypervisor_instname
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_JOB == "kayobe-run"

infravm-create:
  needs: ["infravm-setup-inventory"]
  stage: baremetal
  image: python:3.9
  tags:
    - k8srunner
  allow_failure: false
  script:
    - pip3 install ansible requests
    - cp $TECHOPS_KEY $CI_PROJECT_DIR/ska-techops.pem
    - chmod 600 $CI_PROJECT_DIR/ska-techops.pem
    - make orch generate-inventory SECURITY_CHECK=OFF TF_ARGUMENTS="-auto-approve"  DATACENTRE=$DATACENTRE SERVICE=$SERVICE
    - make infravm-create SECURITY_CHECK=OFF PLAYBOOKS_HOSTS=$TF_VAR_instname INFRAVM_HYPERVISOR_HOST=$TF_VAR_hypervisor_instname
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_JOB == "kayobe-run"

infravm-tests:
  needs: ["infravm-create"]
  stage: baremetal
  image: python:3.9
  tags:
    - k8srunner
  allow_failure: false
  script:
    - pip3 install ansible requests
    - cp $TECHOPS_KEY $CI_PROJECT_DIR/ska-techops.pem
    - chmod 600 $CI_PROJECT_DIR/ska-techops.pem
    - make orch generate-inventory SECURITY_CHECK=OFF TF_ARGUMENTS="-auto-approve"  DATACENTRE=$DATACENTRE SERVICE=$SERVICE
    - make infravm-tests SECURITY_CHECK=OFF PLAYBOOKS_HOSTS=$TF_VAR_instname INFRAVM_HYPERVISOR_HOST=$TF_VAR_hypervisor_instname
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_JOB == "kayobe-run"

destroy_seed:
  when: always
  stage: destroy
  image: python:3.9
  tags:
    - k8srunner
  allow_failure: false
  script:
    - pip3 install ansible poetry  openstackclient
    - wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -O terraform.zip
    - unzip terraform.zip && rm terraform.zip && mv terraform /usr/bin/terraform
    - poetry install
    - cp $CLOUDS_YAML $TF_ROOT_DIR/clouds.yaml
    - make orch init SECURITY_CHECK=OFF
    - make orch apply SECURITY_CHECK=OFF TF_ARGUMENTS="-auto-approve"
    - make orch destroy SECURITY_CHECK=OFF TF_ARGUMENTS="-auto-approve"
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $SCHEDULED_JOB == "kayobe-run"

include:
- local: '.gitlab/ci/cronjobs.gitlab-ci.yml'
- project: 'ska-telescope/templates-repository'
  file: 'gitlab-ci/includes/ansible-lint.gitlab-ci.yml'
- project: 'ska-telescope/templates-repository'
  file: 'gitlab-ci/includes/terraform.gitlab-ci.yml'
- project: 'ska-telescope/templates-repository'
  file: 'gitlab-ci/includes/finaliser.gitlab-ci.yml'
