SHELL=/usr/bin/env bash
MAKEFLAGS += --no-print-directory

.PHONY: im-check-env im-setup-secrets im-get-secrets im-edit-secrets im-rotate-secrets-password im-vars export-as-envs playbooks orch im-check-test-env im-test im-test-install im-test-uninstall im-test-reinstall im-help
.DEFAULT_GOAL := help

DATACENTRE ?=
ENVIRONMENT ?=
SERVICE ?=
TF_HTTP_USERNAME ?=
TF_LINT_TARGETS?=$(shell find ./datacentres -name 'terraform.tf' | grep -v ".make" | sed 's/.terraform.tf//' | sort | uniq )

-include .make/base.mk
-include .make/bats.mk
-include .make/terraform.mk
-include .make/python.mk
-include .make/ansible.mk

BASE_PATH?=$(shell cd "$(dirname "$1")"; pwd -P)

ANSIBLE_COLLECTIONS := ./playbooks

# Include environment specific vars and secrets
-include $(BASE_PATH)/PrivateRules.mak

REDFISH_PASSWORD?=

KAYOBE_GATEWAY ?= au-itf-gateway

GITLAB_PROJECT_ID?=39999641
ENVIRONMENT_ROOT_DIR?=$(BASE_PATH)/datacentres/$(DATACENTRE)/$(ENVIRONMENT)
TF_ROOT_DIR?=$(ENVIRONMENT_ROOT_DIR)/orchestration/$(SERVICE)
TF_HTTP_ADDRESS?=https://gitlab.com/api/v4/projects/$(GITLAB_PROJECT_ID)/terraform/state/$(DATACENTRE)-$(ENVIRONMENT)-$(SERVICE)-terraform-state
TF_HTTP_LOCK_ADDRESS?=https://gitlab.com/api/v4/projects/$(GITLAB_PROJECT_ID)/terraform/state/$(DATACENTRE)-$(ENVIRONMENT)-$(SERVICE)-terraform-state/lock
TF_HTTP_UNLOCK_ADDRESS?=https://gitlab.com/api/v4/projects/$(GITLAB_PROJECT_ID)/terraform/state/$(DATACENTRE)-$(ENVIRONMENT)-$(SERVICE)-terraform-state/lock
PLAYBOOKS_ROOT_DIR?=$(ENVIRONMENT_ROOT_DIR)/installation
PLAYBOOKS_DIR= $(PLAYBOOKS_ROOT_DIR)/playbooks/
PLAYBOOKS := $(shell basename -a "" $$(find $(PLAYBOOKS_ROOT_DIR) -type f | grep -E "playbooks/.*.y.*ml"))
TF_INVENTORY_DIR?=$(PLAYBOOKS_ROOT_DIR)
INVENTORY?=$(PLAYBOOKS_ROOT_DIR)/
ANSIBLE_CONFIG?="$(PLAYBOOKS_ROOT_DIR)/ansible.cfg"
ANSIBLE_SSH_ARGS?=-o ControlPersist=30m -o ControlMaster=auto -o StrictHostKeyChecking=no -F $(PLAYBOOKS_ROOT_DIR)/ssh.config
ANSIBLE_COLLECTIONS_PATHS?=$(HOME)/.ansible/collections/ansible_collections:$(BASE_PATH)/ska-ser-ansible-collections:$(BASE_PATH)/ska-ser-ansible-collections/ansible_collections
SKA_COLLECTIONS_PATH:=$(BASE_PATH)/ska-ser-ansible-collections/ansible_collections/ska_collections
CI_PIPELINE_ID ?= $(shell echo "local-$$(whoami)" | head -c 16)

DEFAULT_TEXT_EDITOR?=vi
ANSIBLE_EXTRA_VARS?=--extra-vars 'ska_datacentre=$(DATACENTRE) ska_environment=$(ENVIRONMENT) ska_service=$(SERVICE) ska_ci_pipeline_id=$(CI_PIPELINE_ID) ska_collections_path=$(SKA_COLLECTIONS_PATH)'

## ANSIBLE_SECRETS_PROVIDER must be one of: legacy, plain-text, ansible-vault or hashicorp-vault
# pass datacentre, env (environment is reserved in ansible) and service variables
ANSIBLE_SECRETS_PROVIDER?=legacy
ANSIBLE_SECRETS_PATH?=$(BASE_PATH)/secrets.yml
ANSIBLE_SECRETS_PASSWORD?=

im-check-env: ## Check environment configuration variables for configuration
ifndef DATACENTRE
	$(error DATACENTRE is undefined)
endif
ifndef ENVIRONMENT
	$(error ENVIRONMENT is undefined)
endif

im-check-playbooks: im-check-env
ifndef PLAYBOOKS_HOSTS
	$(error PLAYBOOKS_HOSTS is undefined)
endif

im-check-orch: im-check-env ## Check environment configuration variables for orchestration
ifndef SERVICE
	$(error SERVICE is undefined)
endif
ifndef TF_HTTP_USERNAME
	$(error TF_HTTP_USERNAME is undefined)
endif
ifndef TF_HTTP_PASSWORD
	$(error TF_HTTP_PASSWORD is undefined)
endif

im-setup-secrets: ## Loads secrets as ansible variables
ifndef ANSIBLE_SECRETS_PROVIDER
	$(error ANSIBLE_SECRETS_PROVIDER is undefined)
endif

# legacy provider
ifeq ($(ANSIBLE_SECRETS_PROVIDER),legacy)
# It is used as a legacy approach, by injecting into a 'secrets' variable the values
# present in PrivateRules.mak. The variable names are lower-case representations of what
# is in PrivateRules.mak
# Finally, we can set whatever variable in group_vars/host_vars as:
# some_var: "{{ secrets['lower case of variable in PrivateRules.mak'] }}
ifeq ($(ANSIBLE_SECRETS_PATH),)
	$(error ANSIBLE_SECRETS_PATH is undefined)
endif
ANSIBLE_EXTRA_VARS += --extra-vars @$(ANSIBLE_SECRETS_PATH)
$(shell touch $(BASE_PATH)/PrivateRules.mak; \
	rm $(ANSIBLE_SECRETS_PATH); echo "secrets:" > $(ANSIBLE_SECRETS_PATH); \
	cat $(BASE_PATH)/PrivateRules.mak | \
	grep -v "DATACENTRE=" | \
	grep -v "ENVIRONMENT=" | \
	grep -v "SERVICE=" | \
	grep -v "^#" | \
	grep -v "^  " | \
	grep -v -P '^\t' | \
	grep -v "# ignore" | \
	grep -v "\+\=" | \
	grep "\S" | \
	sed "s#^\([a-zA-Z0-9_-]\+\)[ ]*\(=\|\?=\|:=\)[ ]*\(.*\)#  \1: \3##" | \
	sed "s#^\(  [a-zA-Z0-9_-]\+\):#\L\1:#" \
	>> $(ANSIBLE_SECRETS_PATH) \
)
$(shell chmod 600 $(ANSIBLE_SECRETS_PATH))
im-get-secrets:
	@cat $(ANSIBLE_SECRETS_PATH)
im-edit-secrets:
	@$(DEFAULT_TEXT_EDITOR) PrivateRules.mak
endif

# plain-text provider
ifeq ($(ANSIBLE_SECRETS_PROVIDER),plain-text)
# It should be used to inject an yaml structured set of variables, passed with '--extra-vars'
# Finally, we can set whatever variable in group_vars/host_vars as:
# some_var: "{{ some.yaml.path.in.secrets }}
ifeq ($(ANSIBLE_SECRETS_PATH),)
	$(error ANSIBLE_SECRETS_PATH is undefined)
endif
ANSIBLE_EXTRA_VARS += --extra-vars @$(ANSIBLE_SECRETS_PATH)
$(shell chmod 600 $(ANSIBLE_SECRETS_PATH))
im-get-secrets:
	@cat $(ANSIBLE_SECRETS_PATH)
im-edit-secrets:
	@$(DEFAULT_TEXT_EDITOR) $(ANSIBLE_SECRETS_PATH)
endif

# ansible-vault provider
ifeq ($(ANSIBLE_SECRETS_PROVIDER),ansible-vault)
# This provider uses the ansible vaulted file at $(ANSIBLE_SECRETS_PATH)
# It is encrypted by the password $(ANSIBLE_SECRETS_PASSWORD) and it is decrypted on usage by ansible
# It should be used to inject an yaml structured set of variables, passed with '--extra-vars'
# Finally, we can set whatever variable in group_vars/host_vars as:
# some_var: "{{ some.yaml.path.in.secrets }}
ifeq ($(ANSIBLE_SECRETS_PATH),)
	$(error ANSIBLE_SECRETS_PATH is undefined)
endif
ifeq ($(ANSIBLE_SECRETS_PASSWORD),)
	$(error ANSIBLE_SECRETS_PASSWORD is undefined)
endif
ANSIBLE_EXTRA_VARS += --extra-vars @$(ANSIBLE_SECRETS_PATH) --vault-password-file $(BASE_PATH)/secrets.password
$(shell echo "$(ANSIBLE_SECRETS_PASSWORD)" > $(BASE_PATH)/secrets.password && chmod 600 $(BASE_PATH)/secrets.password)
im-get-secrets:
	@ansible-vault view $(ANSIBLE_SECRETS_PATH) --vault-password-file $(BASE_PATH)/secrets.password
im-edit-secrets:
	@ansible-vault edit $(ANSIBLE_SECRETS_PATH) --vault-password-file $(BASE_PATH)/secrets.password
im-rotate-secrets-password:
	@echo "Rekeying $(ANSIBLE_SECRETS_PATH)"
	@echo "New secrets password: "; read -s SECRETS_PASSWORD; \
		echo "$$SECRETS_PASSWORD" | sed 's#\(.\{3\}\)\(.*\)\(.\{3\}\)#\1*************\3#'; \
		echo "$$SECRETS_PASSWORD" > $(BASE_PATH)/new-secrets.password && chmod 600 $(BASE_PATH)/new-secrets.password; \
		echo "Please update ANSIBLE_SECRETS_PASSWORD to the contents of $(BASE_PATH)/new-secrets.password";
	@ansible-vault rekey --vault-password-file $(BASE_PATH)/secrets.password --new-vault-password-file $(BASE_PATH)/new-secrets.password
endif

# hashicorp-vault provider
ifeq ($(ANSIBLE_SECRETS_PROVIDER),hashicorp-vault)
	$(error Secrets provider 'hashicorp-vault' is undefined)
endif

ENV_VARS ?= DATACENTRE="$(DATACENTRE)" \
	ENVIRONMENT="$(ENVIRONMENT)" \
	SERVICE="$(SERVICE)" \
	TF_HTTP_USERNAME="$(TF_HTTP_USERNAME)" \
	TF_HTTP_PASSWORD="$(TF_HTTP_PASSWORD)" \
	BASE_PATH="$(BASE_PATH)" \
	GITLAB_PROJECT_ID="$(GITLAB_PROJECT_ID)" \
	ENVIRONMENT_ROOT_DIR="$(ENVIRONMENT_ROOT_DIR)" \
	TF_ROOT_DIR="$(TF_ROOT_DIR)" \
	TF_HTTP_ADDRESS="$(TF_HTTP_ADDRESS)" \
	TF_HTTP_LOCK_ADDRESS="$(TF_HTTP_LOCK_ADDRESS)" \
	TF_HTTP_UNLOCK_ADDRESS="$(TF_HTTP_UNLOCK_ADDRESS)" \
	PLAYBOOKS_ROOT_DIR="$(PLAYBOOKS_ROOT_DIR)" \
	TF_INVENTORY_DIR=$(TF_INVENTORY_DIR) \
	INVENTORY="$(INVENTORY)" \
	ANSIBLE_CONFIG="$(ANSIBLE_CONFIG)" \
	ANSIBLE_SSH_ARGS="$(ANSIBLE_SSH_ARGS)" \
	ANSIBLE_COLLECTIONS_PATHS="$(ANSIBLE_COLLECTIONS_PATHS)" \
	ANSIBLE_EXTRA_VARS="$(ANSIBLE_EXTRA_VARS)" \
	TF_VAR_datacentre="$(DATACENTRE)" \
	TF_VAR_environment="$(ENVIRONMENT)" \
	TF_VAR_service="$(SERVICE)" \
	TF_VAR_instname="$(TF_VAR_instname)" \
	TF_VAR_hypervisor_instname="$(TF_VAR_hypervisor_instname)"

im-vars:  ### Current variables
	@echo "";
	@echo -e "\033[32mMain vars:\033[0m"
	@echo "BASE_PATH=$(BASE_PATH)"
	@echo "DATACENTRE=$(DATACENTRE)"
	@echo "ENVIRONMENT=$(ENVIRONMENT)"
	@echo "SERVICE=$(SERVICE)"
	@echo "PLAYBOOKS_HOSTS=$(PLAYBOOKS_HOSTS)"
	@echo "TF_HTTP_USERNAME=$(TF_HTTP_USERNAME)"
	@echo "TF_HTTP_PASSWORD=$(TF_HTTP_PASSWORD)"
	@echo "GITLAB_PROJECT_ID=$(GITLAB_PROJECT_ID)"
	@echo "ENVIRONMENT_ROOT_DIR=$(ENVIRONMENT_ROOT_DIR)"
	@echo "TF_ROOT_DIR=$(TF_ROOT_DIR)"
	@echo "TF_HTTP_ADDRESS=$(TF_HTTP_ADDRESS)"
	@echo "TF_HTTP_LOCK_ADDRESS=$(TF_HTTP_LOCK_ADDRESS)"
	@echo "TF_HTTP_UNLOCK_ADDRESS=$(TF_HTTP_UNLOCK_ADDRESS)"
	@echo "PLAYBOOKS_ROOT_DIR=$(PLAYBOOKS_ROOT_DIR)"
	@echo "INVENTORY=$(INVENTORY)"
	@echo "ANSIBLE_SECRETS_PATH=$(ANSIBLE_SECRETS_PATH)"
	@echo "ANSIBLE_CONFIG=$(ANSIBLE_CONFIG)"
	@echo "ANSIBLE_SSH_ARGS=$(ANSIBLE_SSH_ARGS)"
	@echo "ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS)"
	@echo "ANSIBLE_EXTRA_VARS=$(ANSIBLE_EXTRA_VARS)"
	@echo "";
	@echo -e "\033[32mOrchestration vars:\033[0m";
	@cd ska-ser-orchestration && $(ENV_VARS) $(MAKE) vars;
	@echo "";
	@echo -e "\033[32mInstallation vars:\033[0m";
	@cd ska-ser-ansible-collections && $(ENV_VARS) $(MAKE) ac-vars;
	@echo "";

IM_SETUP_FILTER ?= ansible_distribution

im-setup: ## Test ansible setup
	$(ENV_VARS) ANSIBLE_FILTER_PLUGINS="$(ANSIBLE_FILTER_PLUGINS)" \
	ansible $(PLAYBOOKS_HOSTS) \
		-i $(INVENTORY) \
		-m setup -a filter=$(IM_SETUP_FILTER)

export-as-envs: im-check-env
	@echo 'export $(ENV_VARS)'

# If the first argument is "install"...
ifeq (playbooks,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "playbooks"
  TARGET_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(TARGET_ARGS):;@:)
endif

## TARGET: playbooks
## SYNOPSIS: make playbooks <job-submodule> <target>
## HOOKS: playbooks-pre, playbooks-post
## VARS:
##       none
##
##  Execute target from job (makefile) specified in AC submodule

playbooks-pre:  ## pre playbooks hook

playbooks-post:  ## post playbooks hook

playbooks-main: im-check-env im-setup-secrets im-get-kayobe-module # main target for playooks
	@cd ska-ser-ansible-collections && \
	$(ENV_VARS) ANSIBLE_FILTER_PLUGINS="$(ANSIBLE_FILTER_PLUGINS)" \
	$(MAKE) $(TARGET_ARGS)

playbooks: playbooks-pre playbooks-main playbooks-post ## Access Ansible Collections submodule targets

## TARGET: orch
## SYNOPSIS: make orch <job-submodule> <target>
## HOOKS: orch-pre, orch-post
## VARS:
##       none
##
##  Access Orchestration submodule targets

orch-pre:  ## pre orch hook

orch-post:  ## post orch hook

# If the first argument is "orch"...
ifeq (orch,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "orch"
  TARGET_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(TARGET_ARGS):;@:)
endif

orch-main: im-check-orch ## Access Orchestration submodule targets
	@cd ska-ser-orchestration && $(ENV_VARS) $(MAKE) $(TARGET_ARGS)

orch: orch-pre orch-main orch-post ## Access Orchestration submodule targets

## TARGET: service
## SYNOPSIS: make service <job-submodule> <target>
## HOOKS: service-pre, service-post
## VARS:
##       none
##
##  Access Orchestration submodule targets

service-pre:  ## pre service hook

service-post:  ## post service hook

service-main: im-check-playbooks
	@$(ENV_VARS) $(MAKE) "service-$(TARGET_ARGS)"

# If the first argument is "service"...
ifeq (service,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "service"
  TARGET_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(TARGET_ARGS):;@:)
endif

service: service-pre service-main service-post ## Access service targets

$(addprefix service-, $(subst .yml,, $(PLAYBOOKS))): im-check-playbooks
	$(ENV_VARS) ansible-playbook $(shell find $(PLAYBOOKS_DIR) -type f -name "*$(shell echo $(MAKECMDGOALS) | sed 's#.*-##')*") \
	-i $(INVENTORY) $(ANSIBLE_PLAYBOOK_ARGUMENTS) $(ANSIBLE_EXTRA_VARS) \
	--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)"

## Testing
# TF_VAR_ci_pipeline_id automatically populates ci_pipeline_id Terraform variable
# that is used as a suffix in the Terraform resource names
TEST_ENV_VARS ?= $(ENV_VARS) \
	TF_HTTP_USERNAME="username" \
	TF_HTTP_PASSWORD="password" \
	TF_VAR_ci_pipeline_id="$(CI_PIPELINE_ID)"

# End-to-end variables
BATS_TESTS_DIR ?= $(BASE_PATH)/tests/e2e
BATS_TEST_SUITES ?= unit setup $(SERVICE) cleanup
SKIP_BATS_TESTS = $(shell [ ! -d $(BATS_TESTS_DIR) ] && echo "true" || echo "false")
BATS_CORE_VERSION = v1.8.0

im-check-test-env:
ifeq ($(SKIP_BATS_TESTS),true)
	@echo "No tests found on $(BATS_TESTS_DIR)"
endif
ifndef DATACENTRE
	$(error DATACENTRE is undefined)
endif
ifndef ENVIRONMENT
	$(error ENVIRONMENT is undefined)
endif
ifndef SERVICE
	$(error SERVICE is undefined);
endif

im-get-kayobe-module:  ## Calculate paths for generated inventory from Kayobe
	$(eval KAYOBE_LIB := $(shell python3 -c 'import os; import kayobe; print(os.path.dirname(kayobe.__file__))'))
	$(eval KAYOBE_SHARE := $(KAYOBE_LIB)/../../../../share/kayobe/ansible/filter_plugins)
	$(eval ANSIBLE_FILTER_PLUGINS := $(KAYOBE_SHARE))

im-cache-kayobe-inventory:  # Build generated inventory from Kayobe
	if [ -f $(PLAYBOOKS_ROOT_DIR)/kayobe_inventory.sh ]; then \
	echo "Using cached kayobe inventory !"; \
	else \
	echo "#!/bin/sh" > $(PLAYBOOKS_ROOT_DIR)/kayobe_inventory.sh; \
	echo "cat $(PLAYBOOKS_ROOT_DIR)/kayobe_inventory.json.txt" >> $(PLAYBOOKS_ROOT_DIR)/kayobe_inventory.sh; \
	ssh $(ANSIBLE_SSH_ARGS) $(KAYOBE_GATEWAY) \
	/home/ubuntu/kayobe-venv/bin/ansible-inventory \
	--inventory /home/ubuntu/kayobe/ansible/inventory \
	--inventory /home/ubuntu/kayobe/config/src/kayobe-config/etc/kayobe/inventory \
	-e @/home/ubuntu/kayobe/config/src/kayobe-config/etc/kayobe/globals.yml \
	-e @/home/ubuntu/kayobe/config/src/kayobe-config/etc/kayobe/infra-vms.yml \
	-e @/home/ubuntu/kayobe/config/src/kayobe-config/etc/kayobe/network-allocation.yml \
	-e @/home/ubuntu/kayobe/config/src/kayobe-config/etc/kayobe/networks.yml \
	-e @/home/ubuntu/kayobe/config/src/kayobe-config/etc/kayobe/overcloud.yml \
	-e @/home/ubuntu/kayobe/config/src/kayobe-config/etc/kayobe/ssh.yml \
	--list > \
	$(PLAYBOOKS_ROOT_DIR)/kayobe_inventory.json.txt; \
	echo "Built inventory: $(PLAYBOOKS_ROOT_DIR)/kayobe_inventory.sh"; \
	chmod a+x $(PLAYBOOKS_ROOT_DIR)/kayobe_inventory.sh; \
	fi

im-remove-kayobe-inventory:  ## Remove generated inventory from Kayobe
	@rm -f $(PLAYBOOKS_ROOT_DIR)/kayobe_inventory.sh
	@rm -f $(PLAYBOOKS_ROOT_DIR)/kayobe_inventory.json.txt

im-rebuild-kayobe-cache: im-remove-kayobe-inventory im-cache-kayobe-inventory  ## Rebuild generated inventory from Kayobe

im-check-inventory: im-get-kayobe-module  ## Test generated inventory from Kayobe
	@echo "KAYOBE_LIB=$(KAYOBE_LIB)"
	@echo "KAYOBE_SHARE=$(KAYOBE_SHARE)"
	ANSIBLE_SSH_ARGS="$(ANSIBLE_SSH_ARGS)" \
	ANSIBLE_FILTER_PLUGINS="$(ANSIBLE_FILTER_PLUGINS)" \
	ansible -i $(INVENTORY) $(KAYOBE_GATEWAY),$(PLAYBOOKS_HOSTS) -m setup -a filter=ansible_default_ipv4

im-test: im-check-test-env
	@if [ ! -d $(BATS_TESTS_DIR)/scripts/bats-core ]; then make --no-print-directory im-test-install; fi
	@$(TEST_ENV_VARS) BASE_DIR=$(BATS_TESTS_DIR) BATS_TEST_TARGETS="$(BATS_TEST_SUITES)" $(MAKE) --no-print-directory bats-test

im-test-cleanup: im-check-test-env
	@if [ ! -d $(BATS_TESTS_DIR)/scripts/bats-core ]; then make --no-print-directory im-test-install; fi
	@$(TEST_ENV_VARS) BASE_DIR=$(BATS_TESTS_DIR) BATS_TEST_TARGETS="cleanup" $(MAKE) --no-print-directory bats-test

im-test-install: im-check-test-env
	@$(TEST_ENV_VARS) BASE_DIR=$(BATS_TESTS_DIR) $(MAKE) --no-print-directory bats-install

im-test-uninstall: im-check-test-env
	@$(TEST_ENV_VARS) BASE_DIR=$(BATS_TESTS_DIR) $(MAKE) --no-print-directory bats-uninstall

im-test-reinstall: im-test-uninstall im-test-install

im-help:  ## Show Help
	@echo "";
	@echo -e "\033[32mBase Vars:\033[0m"
	@make im-vars;
	@echo "";
	@echo -e "\033[32mMain Targets:\033[0m"
	@make help
	@echo "";
	@echo -e "\033[32mOrchestration targets - make orch <target>:\033[0m";
	@cd ska-ser-orchestration && make help-print-targets;
	@echo "";
	@echo -e "\033[32mInstallation targets - make playbooks <target>:\033[0m";
	@cd ska-ser-ansible-collections && make help-print-targets;


####### baremetal targets #####

TAGS ?= all
SKIP_TAGS ?= none
REQ_FLAGS ?=

DEFAULT_VARS := -i ${INVENTORY}  \
				--extra-vars "target_hosts=$(PLAYBOOKS_HOSTS)" \
				--extra-vars "infravm_hypervisor_host=${INFRAVM_HYPERVISOR_HOST}" \
				--extra-vars "datacentre_path=${ENVIRONMENT_ROOT_DIR}" \
				--extra-vars @$(BASE_PATH)/secrets.yml

ansible-pre-lint:
	python3 -m pip install ansible-lint==6.15.0

ansible-requirements:
	ansible-galaxy collection install \
	-r requirements.yml $(REQ_FLAGS)

seed-from-nothing: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/seed-from-nothing.yml \
	${DEFAULT_VARS} \
	--tags ${TAGS} --skip-tags ${SKIP_TAGS}

smoke-test-ironic: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/smoke-test-ironic.yml \
	${DEFAULT_VARS} \
	--tags ${TAGS} -vv

tenks-ironic-test: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/tenks-ironic-test.yml \
	${DEFAULT_VARS} \
	--tags ${TAGS}

test-deploy-openstack: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/test-deploy-openstack.yml \
	${DEFAULT_VARS} \
	--tags ${TAGS}

infravm-setup-inventory: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/infravm-setup-inventory.yml \
	${DEFAULT_VARS} \
	--tags ${TAGS}

overcloud-raidconfig: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/overcloud-raidconfig.yml \
	${DEFAULT_VARS} \
	--extra-vars "redfish_password=$(REDFISH_PASSWORD)" \
	--tags ${TAGS}

overcloud-deploy: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/overcloud-deploy.yml \
	${DEFAULT_VARS} \
	--extra-vars "redfish_password=$(REDFISH_PASSWORD)" \
	--extra-vars "limit_hosts=$(LIMIT_HOSTS)" \
	--tags ${TAGS}

infravm-create: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/infravm-create.yml \
	${DEFAULT_VARS} \
	--tags ${TAGS}

infravm-tests: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/infravm-tests.yml \
	${DEFAULT_VARS} \
	--tags ${TAGS}

monitoring-create-node: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/monitoring-create-node.yml \
	${DEFAULT_VARS} \
	--tags ${TAGS}

monitoring-inventory-setup: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/monitoring-inventory-setup.yml \
	${DEFAULT_VARS} \
	--tags ${TAGS}

infravm-add: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/infravm-add.yml \
	${DEFAULT_VARS} \
	--extra-vars "vm_host_names=$(VM_HOST_NAMES)" \
	--tags ${TAGS}

infravm-remove: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/infravm-remove.yml \
	${DEFAULT_VARS} \
	--extra-vars "vm_host_names=$(VM_HOST_NAMES)" \
	--tags ${TAGS}

infravm-config: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/infravm-config.yml \
	${DEFAULT_VARS} \
	--tags ${TAGS}

configure-hosts: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/configure-hosts.yml \
	${DEFAULT_VARS} \
	--tags ${TAGS}

copy-shutdown-startup-files: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/shutdown_startup.yml \
	${DEFAULT_VARS} \
	--tags ${TAGS} -vvv

startup: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/startup.yml \
	${DEFAULT_VARS} \
	--extra-vars "redfish_password=$(REDFISH_PASSWORD)" \
	--tags ${TAGS} -vvv

seed-ironic-backup: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/seed-backup-restore.yml \
	${DEFAULT_VARS} \
	--tags seed-ironic-backup

seed-ironic-restore: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/seed-backup-restore.yml \
	${DEFAULT_VARS} \
	--extra-vars "restore_timestamp=$(RESTORE_TIMESTAMP)" \
	--tags seed-ironic-restore

seed-vm-backup: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/seed-backup-restore.yml \
	${DEFAULT_VARS} \
	--tags seed-vm-backup

seed-vm-restore: im-check-env ansible-requirements
	${ENV_VARS} ansible-playbook playbooks/seed-backup-restore.yml \
	${DEFAULT_VARS} \
	--extra-vars "restore_timestamp=$(RESTORE_TIMESTAMP)" \
	--tags seed-vm-restore


# kayobe playbook run $HOME/kayobe/ansible/lvm.yml --limit au-itf-aavs301


deploy-openstack: seed-from-nothing smoke-test-ironic tenks-ironic-test test-deploy-openstack

deploy-monitoring:
	make playbooks oci docker
	make playbooks monitoring node-exporter
	make playbooks monitoring prometheus
	make playbooks monitoring alertmanager
	make playbooks monitoring thanos
	make playbooks monitoring grafana

deploy-ceph:
	make playbooks ac-install-dependencies
	make playbooks common update-hosts
	make playbooks oci docker
	make playbooks ceph install

# deploy all of K8s and hardware integration for AAVS3 on both servers
deploy-aavs3-k8s: DATACENTRE := low-itf ENVIRONMENT := production SERVICE := kayobe PLAYBOOKS_HOSTS := aavs3
deploy-aavs3-k8s:
	make playbooks k8s device-integration
	make playbooks k8s install-base
	make playbooks k8s manual-deployment TAGS=k8s
	make playbooks k8s deploy-singlenode
	make playbooks clusterapi calico-install \
		K8S_KUBECONFIG=/etc/kubernetes/admin.conf ANSIBLE_PLAYBOOK_ARGUMENTS+=--become
	make playbooks k8s install \
		TAGS=metallb,standardprovisioner,externaldns,metrics,localpvs \
		K8S_KUBECONFIG=/etc/kubernetes/admin.conf ANSIBLE_PLAYBOOK_ARGUMENTS+=--become
	make playbooks k8s install \
		TAGS=ingress,generic_device_plugin,spookd_device_plugin \
		K8S_KUBECONFIG=/etc/kubernetes/admin.conf ANSIBLE_PLAYBOOK_ARGUMENTS+=--become
	make playbooks k8s install \
		TAGS=ping,taranta \
		K8S_KUBECONFIG=/etc/kubernetes/admin.conf ANSIBLE_PLAYBOOK_ARGUMENTS+=--become
	make playbooks k8s install \
		TAGS=eda \
		K8S_KUBECONFIG=/etc/kubernetes/admin.conf ANSIBLE_PLAYBOOK_ARGUMENTS+=--become
	make playbooks k8s install TAGS=multihoming \
		K8S_KUBECONFIG=/etc/kubernetes/admin.conf ANSIBLE_PLAYBOOK_ARGUMENTS+=--become
	make playbooks k8s install TAGS=nvidia \
		K8S_KUBECONFIG=/etc/kubernetes/admin.conf ANSIBLE_PLAYBOOK_ARGUMENTS+=--become
