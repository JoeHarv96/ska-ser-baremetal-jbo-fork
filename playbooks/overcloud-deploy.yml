---
- name: Deploy overcloud
  hosts: "{{ target_hosts }}"
  gather_facts: true
  vars:
    target_hosts: localhost
    kayobe_limit_hosts: "{{ limit_hosts | default('all', true) }}"
    baremetal_nodes_map:
      au-itf-cloud01: "https://10.135.0.10"
      au-itf-cloud02: "https://10.135.0.11"
      au-itf-cloud03: "https://10.135.0.12"
      au-itf-cloud04: "https://10.135.0.13"
      au-itf-cloud05: "https://10.135.0.14"
      au-itf-ceph01: "https://10.135.0.15"
      au-itf-ceph02: "https://10.135.0.16"
      au-itf-ceph03: "https://10.135.0.17"
      au-itf-aavs301: "https://10.135.0.19"
      au-itf-aavs302: "https://10.135.0.20"
  tasks:
    - name: "Get baremetal node names"
      ansible.builtin.shell: |
        source kayobe-env &&
        baremetal node list -f value -c Name
      register: raidconfig_hosts
      changed_when: false
      args:
        executable: /bin/bash
      environment:
        OS_CLOUD: "bifrost"
      tags:
        - all
        - overcloud-password
        - overcloud-discover-inspect
        - overcloud-provision

    - name: "Set iDRAC attributes"
      ansible.builtin.shell: |
        source kayobe-env &&
        baremetal node set {{ item }} \
        --driver-info redfish_password={{ redfish_password }} \
        --vendor-interface idrac-redfish --driver idrac \
        --driver-info redfish_username=root \
        --driver-info redfish_address={{ baremetal_nodes_map[item] }} \
        --driver-info redfish_verify_ca=False \
        --driver-info redfish_system_id=/redfish/v1/Systems/System.Embedded.1 \
        --bios-interface idrac-redfish \
        --inspect-interface inspector \
        --management-interface idrac-redfish \
        --power-interface idrac-redfish \
        --raid-interface idrac-redfish
      changed_when: false
      args:
        executable: /bin/bash
      environment:
        OS_CLOUD: "bifrost"
      with_items: "{{ raidconfig_hosts.stdout_lines }}"
      tags:
        - all
        - overcloud-password
        - overcloud-discover-inspect

    - name: Map Servers Into Inventory Groups
      ansible.builtin.shell: |
        source kayobe-env &&
        kayobe overcloud inventory discover --limit {{ kayobe_limit_hosts }} > overclouddiscover.txt
      args:
        executable: /bin/bash
      changed_when: false
      tags:
        - all
        - overcloud-discover
        - overcloud-discover-inspect

    - name: Inspect the Overcloud Hardware
      ansible.builtin.shell: |
        source kayobe-env &&
        kayobe overcloud hardware inspect --limit {{ kayobe_limit_hosts }} > overcloudinspect.txt
      args:
        executable: /bin/bash
      changed_when: false
      tags:
        - all
        - overcloud-inspect
        - overcloud-discover-inspect

    - name: "Set root device hint"
      ansible.builtin.shell: |
        source kayobe-env &&
        baremetal node set {{ item }} \
        --property root_device='{"rotational": true, "size": "<= 8000"}'
      changed_when: false
      args:
        executable: /bin/bash
      environment:
        OS_CLOUD: "bifrost"
      with_items: "{{ raidconfig_hosts.stdout_lines }}"
      tags:
        - all
        - overcloud-provision

    - name: Provision Overcloud Hosts
      ansible.builtin.shell: |
        source kayobe-env &&
        kayobe overcloud provision --limit {{ kayobe_limit_hosts }} > overcloudprovision.txt
      args:
        executable: /bin/bash
      changed_when: false
      tags:
        - all
        - overcloud-provision

    - name: Configure Overcloud Hosts
      ansible.builtin.shell: |
        source kayobe-env &&
        kayobe overcloud host configure --limit {{ kayobe_limit_hosts }} > overcloudconfigure.txt
      args:
        executable: /bin/bash
      changed_when: false
      tags:
        - all
        - overcloud-configure

    - name: Configure Hypervisors libvirt hosts
      ansible.builtin.shell: |
        source kayobe-env &&
        kayobe playbook run $KAYOBE_CONFIG_PATH/ansible/hypervisors-libvirt-hosts.yml > overcloudlibvirthosts.txt
      args:
        executable: /bin/bash
      changed_when: false
      tags:
        - never
        - overcloud-libvirt
