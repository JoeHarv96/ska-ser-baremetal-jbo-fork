module "openstack-instance" {
  source   = "../../../../../ska-ser-orchestration/openstack-instance"
  defaults = var.defaults
  providers = {
    openstack = openstack
  }

  configuration = {
    name = "bang-4-terraform-test"
    master = {
      data_volume_size   = 250,
      docker_volume_size = 20
    }
    data = {
      size = 0
    }
    kibana = {
      size = 0
    }
  }
}
