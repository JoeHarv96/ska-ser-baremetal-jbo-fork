module "infravm-instance" {
  source   = "../../../../../ska-ser-orchestration/openstack-instance"
  defaults = var.defaults
  providers = {
    openstack = openstack
  }

  configuration = {
    name            = var.hypervisor_instname
    security_groups = ["default"]

  }
}
