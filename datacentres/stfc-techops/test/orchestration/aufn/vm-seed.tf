module "seed-instance" {
  source   = "../../../../../ska-ser-orchestration/openstack-instance"
  defaults = var.defaults
  providers = {
    openstack = openstack
  }

  configuration = {
    name            = var.instname
    flavor          = "l3.tiny"
    security_groups = ["default"]
  }
}
