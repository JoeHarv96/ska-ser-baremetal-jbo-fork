module "gateway" {
  source   = "../../../../../ska-ser-orchestration/openstack-instance"
  defaults = var.defaults
  providers = {
    openstack = openstack
  }

  configuration = {
    name         = join("-", [var.datacentre, var.environment, var.service])
    applications = ["reverseproxy", "openvpn", "docker_exporter", "node_exporter"]
  }
}
