# OpenStack Cloud Configurations
openstack = {
  cloud      = "openstack"
  project_id = "b5069610560e42bb837183ad5cd58ec0"
}

# OpenStack Instance defaults for the given OpenStack Cloud
defaults = {
  flavor            = "c3.medium"
  image             = "ubuntu-jammy-22.04"
  availability_zone = "ceph"
  network           = "SKA-TechOps-Private"
  keypair           = "ska-techops"
  # floating_ip_network = "External"
  vpn_cidr_blocks = ["0.0.0.0/0"]
}
