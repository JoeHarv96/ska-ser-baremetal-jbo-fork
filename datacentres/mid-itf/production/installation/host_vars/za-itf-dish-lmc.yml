---
#defaults for the eda
timescaledb_namespace: ska-tango-archiver
timescaledb_version: 0.1.2
timescaledb_values: {replicaCount: 1, 'persistentVolumes': {'data': {'size': '47Gi'}, 'wal': {'size': '3Gi'}},'resources': {'limits': {'cpu': '2', 'memory': '2Gi'},'requests': {'cpu': '2', 'memory': '2Gi'}}}

k8s_kubernetes_version: 1.26.4

# sorry! - had to bump it for 22.04 :-((
# Don't bump this - 5.X removes the disable_raw_qp_enforcement option
# which makes it easier to develop and run code as a non-root user.
activate_mellanox: false
# see ska-ser-ansible-collections/resources/jobs/k8s.mk as these overides do not work for
# vars/main.yml in 3rd party haggaie.mlnx_ofed
mlnx_ofed_version: 5.9-0.5.6.0
mlnx_ofed_distro: ubuntu22.04
# https://www.mellanox.com/page/mlnx_ofed_eula?mtag=linux_sw_drivers&mrequest=downloads&mtype=ofed&mver=MLNX_OFED-5.9-0.5.6.0&mname=MLNX_OFED_LINUX-5.9-0.5.6.0-ubuntu22.04-x86_64.iso

# to be populated when known for aavs3
# sriov_dp_resource_list:
#   - resourcePrefix: skao.int
#     resourceName: low_sdn
#     selectors:
#       vendors: ["15b3"]
#       "pfNames": ["enp65s0f0"]

activate_nvidia: false
k8s_activate_nvidia: "{{ activate_nvidia }}"
docker_activate_nvidia: "{{ activate_nvidia }}"
containerd_activate_nvidia: "{{ activate_nvidia }}"
k8s_install_ceph_drivers: false
k8s_install_kubelet: true
xilinx_xrt_install: false
xilinx_video_sdk_install: true

k8s_kubeconfig: /etc/kubernetes/admin.conf
k8s_ingress_lb_suffix: dish-lmc
k8s_externaldns_lb_suffix: dish-lmc
k8s_dns_servers: [10.164.0.11]
k8s_metallb_addresses: "10.164.254.1-10.164.254.99"
k8s_cluster_api_address: "{{ hostvars[inventory_hostname].ansible_default_ipv4.address }}"
k8s_cluster_hostname: dish-lmc.za-itf.skao.int
k8s_externaldns_lb_ip: ""
k8s_ingress_lb_ip: ""

k8s_cluster_domain: za-dish-lmc.skao.int
k8s_externaldns_cluster_domain: "{{ k8s_cluster_domain }}"
k8s_dns_entry: dish-lmc.za-itf.skao.int
k8s_pod_network_cidr: 10.10.0.0/16
k8s_ingress_lb_class: metallb
k8s_external_service_type: ClusterIP
k8s_externaldns_coredns_lb_class: "{{ k8s_ingress_lb_class }}"

calico_ipv4pool_cidr: "{{ k8s_pod_network_cidr }}"
calico_ipv4pool_ipip: Never
calico_ip_autodetection_interface: "ens3"

# minikube_start_user: root
# minikube_driver: none
# minikube_runtime: containerd
# minikube_mount:
# minikube_calico: true

# minikube_addons:
#   - "logviewer"
#   - "ingress"
#   - "csi-hostpath-driver"
#   - " metrics-server"

# these need specific configuration relating to devices on the final machine placement
k8s_spookd_device_plugin: true
k8s_generic_device_plugin: false

# Monitoring needs to set CPUs, which requires docker python package and not docker-py
docker_python_pkg: docker


#### MONITORING

# minikube_additional_names:
#   - "aavs3"

# minikube_additional_ips:
#   - "127.0.0.1"

prometheus_k8s_api_server_addr: "10.164.0.44"

prometheus_localuser: svc_skadev_ansible
monitoring_localgroup: admin

prometheus_alertmanager_url: http://10.165.4.44:9093
prometheus_url: https://10.165.4.44:9090
slack_channel: alerts-za-itf-dish-lmc
slack_channel_user: alerts-za-itf-dish-lmc-user

prometheus_k8s_external_dns_entry: k8s.za-itf-dish-lmc.skao.int

project_name: "DISH-LMC"

prometheus_datacentre: dish-lmc-monitor

prometheus_server_kubectl_version: 'v1.25.5'

prometheus_openstack_exporter_addr: ""

prometheus_snpm_exporter_enabled: true

grafana_datasource_url: https://10.165.4.44:9090
grafana_ceph_enabled: false
grafana_elasticstack_enabled: false
grafana_kubernetes_enabled: true
grafana_gitlab_runners_enabled: false
grafana_cadvisor_enabled: true
grafana_node_exporter_enabled: true
grafana_openstack_exporter_enabled: false
grafana_idrac_exporter_enabled: false
grafana_riello_ups_exporter_enabled: false
grafana_alerts_enabled: true

thanos_query_endpoints: []

prometheus_static_node_metric_relabel_configs:
  - action: replace
    regex: 10\.164\.0\.44:9100
    replacement: za-itf-dish-lmc:9100
    source_labels:
      - instance
    target_label: instance
  - action: replace
    regex: 10\.164\.0\.44:9323
    replacement: za-itf-dish-lmc:9323
    source_labels:
      - instance
    target_label: instance
  - action: replace
    regex: 10\.164\.0\.44:10249
    replacement: za-itf-dish-lmc:10249
    source_labels:
      - instance
    target_label: instance

prometheus_scrape_configs:
  - job_name: "kube-proxy"
    static_configs:
      - targets:
        - "10.164.0.44:10249"
    metric_relabel_configs: "{{ prometheus_static_node_metric_relabel_configs +
      prometheus_node_metric_relabel_configs }}"
    relabel_configs:
      - source_labels: ['__name__']
        regex: '.*'
        action: replace
        target_label: 'datacentre'
        replacement: "{{ prometheus_datacentre }}"

  - job_name: "kube-state-metrics"
    static_configs:
      - targets:
        - "10.164.0.44:32080"  # load balancer
    relabel_configs:
      - source_labels: ['__name__']
        regex: '.*'
        action: replace
        target_label: 'datacentre'
        replacement: "{{ prometheus_datacentre }}"

  - job_name: "k8stelemetry"
    static_configs:
      - targets:
        - "10.164.0.44:32081"  # load balancer
    relabel_configs:
      - source_labels: ['__name__']
        regex: '.*'
        action: replace
        target_label: 'datacentre'
        replacement: "{{ prometheus_datacentre }}"

  - job_name: "prometheus"
    scheme: https
    tls_config:
      insecure_skip_verify: true
    static_configs:
      - targets:
          - "{{ ansible_fqdn | default(ansible_host) | default('localhost') }}:9090"

  - job_name: "node"
    static_configs:
      - targets:
        - "10.164.0.44:9100"
    metric_relabel_configs: "{{ prometheus_static_node_metric_relabel_configs +
       prometheus_node_metric_relabel_configs }}"
    relabel_configs:
      - source_labels: ['__name__']
        regex: '.*'
        action: replace
        target_label: 'datacentre'
        replacement: "{{ prometheus_datacentre }}"

  - job_name: "docker"
    static_configs:
      - targets:
        - "10.164.0.44:9323"
    relabel_configs:
      - source_labels: ['__name__']
        regex: '.*'
        action: replace
        target_label: 'datacentre'
        replacement: "{{ prometheus_datacentre }}"


### Server Resources & Configuration

## Grafana
grafana_server_cpus: 0.3
grafana_server_memory: 256M
grafana_server_memory_swap: unlimited
grafana_server_memory_swappiness: 60
# # Fine-tuning
# grafana_quota_enabled: true
# grafana_quota_global_session: 1

## Thanos
thanos_sidecar_server_cpus: 0.1
thanos_sidecar_server_memory: 32M
thanos_sidecar_server_memory_swap: unlimited
thanos_sidecar_server_memory_swappiness: 60

## Prometheus
prometheus_server_cpus: 0.3
prometheus_server_memory: 512M
prometheus_server_memory_swap: unlimited
prometheus_server_memory_swappiness: 60
# Fine-tuning
prometheus_server_swapfile_fallocate: false  # AAVS3 nodes already have swap

## Alertmanager
alertmanager_server_cpus: 0.1
alertmanager_server_memory: 32M
alertmanager_server_memory_swap: unlimited
alertmanager_server_memory_swappiness: 60

## Filebeat
logging_filebeat_server_cpus: 0.1
logging_filebeat_server_memory: 80M
logging_filebeat_server_memory_swap: unlimited
logging_filebeat_server_memory_swappiness: 60
# Fine-tuning
logging_filebeat_queue_disk_enabled: true
logging_filebeat_queue_disk_max_size: 10GB

## SNMP
snmp_server_cpus: 0.1
snmp_server_memory: 16M
snmp_server_memory_swap: unlimited
snmp_server_memory_swappiness: 60

