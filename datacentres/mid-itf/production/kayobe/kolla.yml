---
# Kayobe Kolla configuration.

###############################################################################
# Kolla installation.

# Type of Kolla control installation. One of 'binary' or 'source'.
#kolla_ctl_install_type:

# Path to directory for kolla source code checkout.
#kolla_source_path:

# URL of Kolla source code repository if type is 'source'.
#kolla_source_url:

# Version (branch, tag, etc.) of Kolla source code repository if type is
# 'source'. Default is {{ openstack_branch }}.
#kolla_source_version:

# Path to virtualenv in which to install kolla.
#kolla_venv:

# Path in which to generate kolla configuration.
#kolla_build_config_path:

###############################################################################
# Kolla-ansible installation.

# Type of Kolla-ansible control installation. One of 'binary' or 'source'.
# Default is 'source'.
#kolla_ansible_ctl_install_type:

# Path to directory for kolla-ansible source code checkout.
# Default is $KOLLA_SOURCE_PATH, or $PWD/src/kolla-ansible if
# $KOLLA_SOURCE_PATH is not set.
#kolla_ansible_source_path:

# URL of Kolla Ansible source code repository if type is 'source'. Default is
# https://opendev.org/openstack/kolla-ansible.
#kolla_ansible_source_url:

# Version (branch, tag, etc.) of Kolla Ansible source code repository if type
# is 'source'. Default is {{ openstack_branch }}.
#kolla_ansible_source_version:

# Path to virtualenv in which to install kolla-ansible. Default is
# $KOLLA_VENV_PATH or $PWD/venvs/kolla-ansible if $KOLLA_VENV_PATH is not set.
#kolla_ansible_venv:

# Extra requirements to install inside the kolla-ansible virtualenv.
#kolla_ansible_venv_extra_requirements:

# Pip requirement specifier for the ansible package. NOTE: This limits the
# version of ansible used by kolla-ansible to avoid new releases from breaking
# tested code. Changes to this limit should be tested.
#kolla_ansible_venv_ansible:

# Path to Kolla-ansible configuration directory. Default is $KOLLA_CONFIG_PATH
# or /etc/kolla if $KOLLA_CONFIG_PATH is not set.
#kolla_config_path:

# Path to Kolla-ansible node custom configuration directory. Default is
# {{ kolla_config_path }}/config.
#kolla_node_custom_config_path:

###############################################################################
# Kolla configuration.

# Kolla base container image distribution. Options are "centos", "debian",
# "rocky", "ubuntu". Default is {{ os_distribution }}.
#kolla_base_distro:

# Kolla base container image distribution version default map.
# Defines default versions for each distribution.
#kolla_base_distro_version_default_map:

# Kolla base container image distribution version.
# Default is kolla_base_distro_version_default_map[kolla_base_distro].
#kolla_base_distro_version:

# URL of docker registry to use for Kolla images. Default is not set, in which
# case Dockerhub will be used.
#kolla_docker_registry:

# Docker namespace to use for Kolla images. Default is 'kolla'.
#kolla_docker_namespace:

# Whether docker should be configured to use an insecure registry for Kolla
# images. Default is false, unless docker_registry_enabled is true and
# docker_registry_enable_tls is false.
#kolla_docker_registry_insecure:

# Username to use to access a docker registry. Default is not set, in which
# case the registry will be used without authentication.
#kolla_docker_registry_username:

# Password to use to access a docker registry. Default is not set, in which
# case the registry will be used without authentication.
#kolla_docker_registry_password:

# Kolla OpenStack release version. This should be a Docker image tag.
# Default is {{ openstack_release }}.
#kolla_openstack_release:

# Docker tag applied to built container images. Default is {{
# kolla_openstack_release }}-{{ kolla_base_distro }}-{{
# kolla_base_distro_version }}.
#kolla_tag:

# Dict mapping names of sources to their definitions.
# See kolla.common.config for details.
# Example:
# kolla_sources:
#   ironic-base:
#     type: git
#     location: https://github.com/openstack/ironic
#     reference: master
#kolla_sources:

###############################################################################
# Kolla image build configuration.

# Dict mapping Jinja2 block names in kolla's Docker images to their contents.
#kolla_build_blocks:

# Customise the bifrost-deploy image so that it has the updated susy drivers
# rebuild and push:
#   kayobe overcloud container image build '^bifrost-deploy' --push
kolla_build_blocks:
  bifrost_deploy_footer: |
    {% raw %}
    RUN /var/lib/kolla/venv/bin/pip3 install git+https://opendev.org/openstack/sushy.git@master \
      && /var/lib/kolla/venv/bin/pip3 install sushy-oem-idrac
    {% endraw %}

# Dict mapping image customization variable names to their values.
# Each variable takes the form:
# <image name>_<customization>_<operation>
# Hyphens in the image name must be replaced with underscores. The
# customization is most commonly packages. The operation should be one of
# override, append or remove. The value should be a list.
#kolla_build_customizations:

###############################################################################
# Kolla-ansible inventory configuration.

# Full custom seed inventory contents.
#kolla_seed_inventory_custom:

# List of names of default host variables to pass through from kayobe hosts to
# the kolla-ansible seed host, if set. See also
# kolla_seed_inventory_pass_through_host_vars_map.
#kolla_seed_inventory_pass_through_host_vars_default:

# List of names of additional host variables to pass through from kayobe hosts
# to the kolla-ansible seed host, if set. See also
# kolla_seed_inventory_pass_through_host_vars_map.
#kolla_seed_inventory_pass_through_host_vars_extra:

# List of names of host variables to pass through from kayobe hosts to
# the kolla-ansible seed host, if set. See also
# kolla_seed_inventory_pass_through_host_vars_map.
#kolla_seed_inventory_pass_through_host_vars:

# Dict mapping names of default variables in
# kolla_seed_inventory_pass_through_host_vars to the variable to use in
# kolla-ansible. If a variable name is not in this mapping the kayobe name is
# used.
#kolla_seed_inventory_pass_through_host_vars_map_default:

# Dict mapping names of extra variables in
# kolla_seed_inventory_pass_through_host_vars to the variable to use in
# kolla-ansible. If a variable name is not in this mapping the kayobe name is
# used.
#kolla_seed_inventory_pass_through_host_vars_map_extra:

# Dict mapping names of variables in
# kolla_seed_inventory_pass_through_host_vars to the variable to use in
# kolla-ansible. If a variable name is not in this mapping the kayobe name is
# used.
#kolla_seed_inventory_pass_through_host_vars_map:

# Custom overcloud inventory containing a mapping from top level groups to
# hosts.
#kolla_overcloud_inventory_custom_top_level:

# Custom overcloud inventory containing a mapping from components to top level
# groups.
#kolla_overcloud_inventory_custom_components:

# Custom overcloud inventory containing a mapping from services to components.
#kolla_overcloud_inventory_custom_services:

# Full custom overcloud inventory contents. By default this will be the
# concatenation of the top level, component, and service inventories.
#kolla_overcloud_inventory_custom:

# Dict mapping from kolla-ansible groups to kayobe groups and variables. Each
# item is a dict with the following items:
# * groups: A list of kayobe ansible groups to map to this kolla-ansible group.
# * vars: A dict mapping variable names to values for hosts in this
#         kolla-ansible group.
#kolla_overcloud_inventory_top_level_group_map:

# List of names of top level kolla-ansible groups. Any of these groups which
# have no hosts mapped to them will be provided with an empty group definition.
#kolla_overcloud_inventory_kolla_top_level_groups:

# List of names of default host variables to pass through from kayobe hosts to
# kolla-ansible hosts, if set. See also
# kolla_overcloud_inventory_pass_through_host_vars_map.
#kolla_overcloud_inventory_pass_through_host_vars_default:

# List of names of additional host variables to pass through from kayobe hosts
# to kolla-ansible hosts, if set. See also
# kolla_overcloud_inventory_pass_through_host_vars_map.
#kolla_overcloud_inventory_pass_through_host_vars_extra:

# List of names of host variables to pass through from kayobe hosts to
# kolla-ansible hosts, if set. See also
# kolla_overcloud_inventory_pass_through_host_vars_map.
#kolla_overcloud_inventory_pass_through_host_vars:

# Dict mapping names of default variables in
# kolla_overcloud_inventory_pass_through_host_vars to the variable to use in
# kolla-ansible. If a variable name is not in this mapping the kayobe name is
# used.
#kolla_overcloud_inventory_pass_through_host_vars_map_default:

# Dict mapping names of additional variables in
# kolla_overcloud_inventory_pass_through_host_vars to the variable to use in
# kolla-ansible. If a variable name is not in this mapping the kayobe name is
# used.
#kolla_overcloud_inventory_pass_through_host_vars_map_extra:

# Dict mapping names of variables in
# kolla_overcloud_inventory_pass_through_host_vars to the variable to use in
# kolla-ansible. If a variable name is not in this mapping the kayobe name is
# used.
#kolla_overcloud_inventory_pass_through_host_vars_map:

###############################################################################
# Kolla-ansible configuration.

# Virtualenv directory where Kolla-ansible's ansible modules will execute
# remotely on the target nodes. If None, no virtualenv will be used.
#kolla_ansible_target_venv:

# Whether TLS is enabled for the external API endpoints. Default is 'no'.
#kolla_enable_tls_external:

# Whether TLS is enabled for the internal API endpoints. Default is 'no'.
#kolla_enable_tls_internal:

# Whether debug logging is enabled. Default is 'false'.
#kolla_openstack_logging_debug:

# Upper constraints file for installation of Kolla.
# Default value is {{ pip_upper_constraints_file }}.
#kolla_upper_constraints_file:

# User account to use for Kolla SSH access. Default is 'kolla'.
#kolla_ansible_user:

# Primary group of Kolla SSH user. Default is 'kolla'.
#kolla_ansible_group:

# Whether to use privilege escalation for all operations performed via Kolla
# Ansible. Default is 'false'.
#kolla_ansible_become:

# Whether to create a user account, configure passwordless sudo and authorise
# an SSH key for Kolla Ansible. Default is 'true'.
#kolla_ansible_create_user:

###############################################################################
# Kolla feature flag configuration.

kolla_enable_aodh: false
kolla_enable_barbican: false
kolla_enable_blazar: false
kolla_enable_ceilometer: false
kolla_enable_ceilometer_horizon_policy_file: false
kolla_enable_ceilometer_ipmi: false
kolla_enable_ceilometer_prometheus_pushgateway: false
kolla_enable_cells: false
kolla_enable_central_logging: false
kolla_enable_ceph_rgw: false
kolla_enable_ceph_rgw_loadbalancer: false
kolla_enable_cinder: false
kolla_enable_cinder_backend_hnas_nfs: false
kolla_enable_cinder_backend_iscsi: false
kolla_enable_cinder_backend_lvm: false
kolla_enable_cinder_backend_nfs: false
kolla_enable_cinder_backend_pure_fc: false
kolla_enable_cinder_backend_pure_iscsi: false
kolla_enable_cinder_backend_pure_roce: false
kolla_enable_cinder_backend_quobyte: false
kolla_enable_cinder_backup: false
kolla_enable_cinder_horizon_policy_file: false
kolla_enable_cloudkitty: false
kolla_enable_collectd: false
kolla_enable_container_healthchecks: false
kolla_enable_cyborg: false
kolla_enable_designate: false
kolla_enable_destroy_images: false
kolla_enable_etcd: false
kolla_enable_external_api_firewalld: false
kolla_enable_external_mariadb_load_balancer: false
kolla_enable_fluentd: false
kolla_enable_freezer: false
kolla_enable_glance: false
kolla_enable_glance_horizon_policy_file: false
kolla_enable_glance_image_cache: false
kolla_enable_gnocchi: false
kolla_enable_gnocchi_statsd: false
kolla_enable_grafana: false
kolla_enable_grafana_external: false
kolla_enable_hacluster: false
kolla_enable_haproxy: false
kolla_enable_haproxy_memcached: false
kolla_enable_heat: false
kolla_enable_heat_horizon_policy_file: false
kolla_enable_horizon: false
kolla_enable_horizon_blazar: false
kolla_enable_horizon_cloudkitty: false
kolla_enable_horizon_designate: false
kolla_enable_horizon_freezer: false
kolla_enable_horizon_heat: false
kolla_enable_horizon_ironic: false
kolla_enable_horizon_magnum: false
kolla_enable_horizon_manila: false
kolla_enable_horizon_masakari: false
kolla_enable_horizon_mistral: false
kolla_enable_horizon_murano: false
kolla_enable_horizon_neutron_vpnaas: false
kolla_enable_horizon_octavia: false
kolla_enable_horizon_sahara: false
kolla_enable_horizon_senlin: false
kolla_enable_horizon_solum: false
kolla_enable_horizon_tacker: false
kolla_enable_horizon_trove: false
kolla_enable_horizon_vitrage: false
kolla_enable_horizon_watcher: false
kolla_enable_horizon_zun: false
kolla_enable_influxdb: false
kolla_enable_ironic: false
kolla_enable_ironic_ipxe: false
kolla_enable_ironic_neutron_agent: false
kolla_enable_ironic_pxe_uefi: false
kolla_enable_iscsid: false
kolla_enable_keepalived: false
kolla_enable_keystone: false
kolla_enable_keystone_federation: false
kolla_enable_keystone_horizon_policy_file: false
kolla_enable_kuryr: false
kolla_enable_loadbalancer: false
kolla_enable_magnum: false
kolla_enable_manila: false
kolla_enable_manila_backend_cephfs_native: false
kolla_enable_manila_backend_cephfs_nfs: false
kolla_enable_manila_backend_generic: false
kolla_enable_manila_backend_glusterfs_nfs: false
kolla_enable_manila_backend_hnas: false
kolla_enable_mariabackup: false
kolla_enable_mariadb: false
kolla_enable_masakari: false
kolla_enable_masakari_hostmonitor: false
kolla_enable_masakari_instancemonitor: false
kolla_enable_memcached: false
kolla_enable_mistral: false
kolla_enable_multipathd: false
kolla_enable_murano: false
kolla_enable_neutron: false
kolla_enable_neutron_agent_ha: false
kolla_enable_neutron_bgp_dragent: false
kolla_enable_neutron_dvr: false
kolla_enable_neutron_horizon_policy_file: false
kolla_enable_neutron_infoblox_ipam_agent: false
kolla_enable_neutron_metering: false
kolla_enable_neutron_mlnx: false
kolla_enable_neutron_packet_logging: false
kolla_enable_neutron_port_forwarding: false
kolla_enable_neutron_provider_networks: false
kolla_enable_neutron_qos: false
kolla_enable_neutron_segments: false
kolla_enable_neutron_sfc: false
kolla_enable_neutron_sriov: false
kolla_enable_neutron_trunk: false
kolla_enable_neutron_vpnaas: false
kolla_enable_nova: false
kolla_enable_nova_fake: false
kolla_enable_nova_horizon_policy_file: false
kolla_enable_nova_libvirt_container: false
kolla_enable_nova_serialconsole_proxy: false
kolla_enable_nova_ssh: false
kolla_enable_octavia: false
kolla_enable_octavia_driver_agent: false
kolla_enable_opensearch: false
kolla_enable_opensearch_dashboards: false
kolla_enable_opensearch_dashboards_external: false
kolla_enable_openstack_core: false
kolla_enable_openvswitch: false
kolla_enable_osprofiler: false
kolla_enable_outward_rabbitmq: false
kolla_enable_ovn: false
kolla_enable_ovs_dpdk: false
kolla_enable_placement: false
kolla_enable_prometheus: false
kolla_enable_prometheus_alertmanager: false
kolla_enable_prometheus_alertmanager_external: false
kolla_enable_prometheus_blackbox_exporter: false
kolla_enable_prometheus_cadvisor: false
kolla_enable_prometheus_ceph_mgr_exporter: false
kolla_enable_prometheus_elasticsearch_exporter: false
kolla_enable_prometheus_etcd_integration: false
kolla_enable_prometheus_fluentd_integration: false
kolla_enable_prometheus_haproxy_exporter: false
kolla_enable_prometheus_libvirt_exporter: false
kolla_enable_prometheus_memcached_exporter: false
kolla_enable_prometheus_msteams: false
kolla_enable_prometheus_mysqld_exporter: false
kolla_enable_prometheus_node_exporter: false
kolla_enable_prometheus_openstack_exporter: false
kolla_enable_prometheus_openstack_exporter_external: false
kolla_enable_prometheus_rabbitmq_exporter: false
kolla_enable_prometheus_server: false
kolla_enable_proxysql: false
kolla_enable_rabbitmq: false
kolla_enable_redis: false
kolla_enable_sahara: false
kolla_enable_senlin: false
kolla_enable_skydive: false
kolla_enable_solum: false
kolla_enable_swift: false
kolla_enable_swift_recon: false
kolla_enable_swift_s3api: false
kolla_enable_tacker: false
kolla_enable_telegraf: false
kolla_enable_trove: false
kolla_enable_trove_singletenant: false
kolla_enable_venus: false
kolla_enable_vitrage: false
kolla_enable_vitrage_prometheus_datasource: false
kolla_enable_watcher: false
kolla_enable_zookeeper: false
kolla_enable_zun: false

###############################################################################
# Passwords and credentials.

# Dictionary containing default custom passwords to add or override in the
# Kolla passwords file.
#kolla_ansible_default_custom_passwords:

# Dictionary containing custom passwords to add or override in the Kolla
# passwords file.
#kolla_ansible_custom_passwords:

###############################################################################
# OpenStack API addresses.

# Virtual IP address of OpenStack internal API. Default is the vip_address
# attribute of the internal network.
kolla_internal_vip_address: 10.165.4.2

# Fully Qualified Domain Name (FQDN) of OpenStack internal API. Default is the
# fqdn attribute of the internal network if set, otherwise
# kolla_internal_vip_address.
#kolla_internal_fqdn:

# Virtual IP address of OpenStack external API. Default is the vip_address
# attribute of the external network.
kolla_external_vip_address: 10.164.0.2

# Fully Qualified Domain Name (FQDN) of OpenStack external API. Default is the
# fqdn attribute of the external network if set, otherwise
# kolla_external_vip_address.
#kolla_external_fqdn:

###############################################################################
# TLS certificate bundle management

# External API certificate bundle.
#
# When kolla_enable_tls_external is true, this should contain an X.509
# certificate bundle for the external API.
#
# Note that this should be formatted as a literal style block scalar.
#kolla_external_tls_cert:

# Path to a CA certificate file to use for the OS_CACERT environment variable
# in public-openrc.sh file when TLS is enabled, instead of Kolla-Ansible's
# default.
#kolla_external_fqdn_cacert:

# Internal API certificate bundle.
#
# When kolla_enable_tls_internal is true, this should contain an X.509
# certificate bundle for the internal API.
#
# Note that this should be formatted as a literal style block scalar.
#kolla_internal_tls_cert:

# Path to a CA certificate file to use for the OS_CACERT environment variable
# in admin-openrc.sh file when TLS is enabled, instead of Kolla-Ansible's
# default.
#kolla_internal_fqdn_cacert:

###############################################################################
# Proxy configuration

# HTTP proxy URL (format: http(s)://[user:password@]proxy_name:port) used by
# Kolla. Default value is "{{ http_proxy }}".
#kolla_http_proxy:

# HTTPS proxy URL (format: http(s)://[user:password@]proxy_name:port) used by
# Kolla. Default value is "{{ https_proxy }}".
#kolla_https_proxy:

# List of domains, hostnames, IP addresses and networks for which no proxy is
# used. Default value is "{{ no_proxy }}".
#kolla_no_proxy:

###############################################################################
# Dummy variable to allow Ansible to accept this file.
workaround_ansible_issue_8743: yes
