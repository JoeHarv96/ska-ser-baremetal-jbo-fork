---
# Storage
lvm_groups: "{{ lvm_groups_with_data }}"

# Suggested list of infra hypervisors volume groups for libvirt. Not used by default.
lvm_groups_with_data:
  - "{{ lvm_group_data }}"

# LVM volume group for data. See mrlesmithjr.manage-lvm role for format.
lvm_group_data:
  vgname: data
  disks: "{{ lvm_group_data_disks }}"
  create: True
  lvnames: "{{ lvm_group_data_lvs }}"

# List of disks for use by infra hypervisors LVM data volume group. Default to an
# invalid value to require configuration.
lvm_group_data_disks:
  - /dev/sdb

# List of LVM logical volumes for the data volume group.
lvm_group_data_lvs:
  - "{{ lvm_group_data_lv_local_storage }}"

# local storage LVM backing volume.
lvm_group_data_lv_local_storage:
  lvname: local-storage
  size: "{{ lvm_group_data_lv_local_storage_size }}"
  create: True
  filesystem: "{{ lvm_group_data_lv_local_storage_fs }}"
  mount: True
  mntp: "{{ local_pool_path }}"

# Size of local storage LVM backing volume.
lvm_group_data_lv_local_storage_size: 1T

# Filesystem for local storage LVM backing volume. ext4 allows for shrinking.
lvm_group_data_lv_local_storage_fs: ext4

# Mount point for local storage fs
local_pool_path: /var/lib/csi-hostpath-data
