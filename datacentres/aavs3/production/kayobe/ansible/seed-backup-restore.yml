---
- name: Backup the Seed Ironic Database
  gather_facts: true
  hosts: localhost
  tags:
    - seed-ironic-backup
    - never
  vars:
    target_hosts: localhost
    datacenter: low
    backup_folder: /tmp/seed-backups
  tasks:
    - name: Save current timestamp on a variable
      ansible.builtin.set_fact:
        current_time: "{{ ansible_date_time.epoch }}"

    - name: Stop, backup, start ironic services
      block:
        - name: Assert that the UPS is powered
          ansible.builtin.shell: snmpwalk -Oqv -c {{ snmp_ups_auth_community }} -v {{ snmp_ups_version }} {{ snmp_ups_ip }} 1.3.6.1.2.1.33.1.2.2
          register: ups_seconds_on_battery
          failed_when: ups_seconds_on_battery.rc != 0 or ups_seconds_on_battery.stdout | int > 0
          when:
            - snmp_ups_ip is defined
            - snmp_ups_ip | length > 0
          changed_when: false

        - name: Assert that the UPS has enough charge remaining
          ansible.builtin.shell: snmpwalk -Oqv -c {{ snmp_ups_auth_community }} -v {{ snmp_ups_version }} {{ snmp_ups_ip }} 1.3.6.1.2.1.33.1.2.3
          register: ups_estimated_minutes_remaining
          failed_when: ups_estimated_minutes_remaining.rc != 0 or ups_estimated_minutes_remaining.stdout | int < 20
          when:
            - snmp_ups_ip is defined
            - snmp_ups_ip | length > 0
          changed_when: false

        - name: Stop the ironic services
          ansible.builtin.shell: |
            source /home/ubuntu/kayobe-env &&
            kayobe seed host command run --command "docker exec bifrost_deploy systemctl stop ironic ironic-inspector"
          args:
            executable: /bin/bash

        - name: Backup the ironic database
          ansible.builtin.shell: |
            source /home/ubuntu/kayobe-env &&
            kayobe seed host command run --command "docker exec bifrost_deploy mysqldump --all-databases --single-transaction --routines --triggers > seed-backup.sql"
          args:
            executable: /bin/bash

        - name: Start the ironic services
          ansible.builtin.shell: |
            source /home/ubuntu/kayobe-env &&
            kayobe seed host command run --command "docker exec bifrost_deploy systemctl start ironic ironic-inspector"
          args:
            executable: /bin/bash

        - name: Create seed backups folder if it doesn't exist
          ansible.builtin.file:
            path: "{{ backup_folder }}"
            state: directory
            mode: "0755"

        - name: Copy the database backup into the seed backups folder
          ansible.builtin.shell: "scp -o StrictHostKeyChecking=no stack@seed:seed-backup.sql {{ backup_folder }}/seed-backup-{{ current_time }}.sql"

        - name: Stat the backup file
          ansible.builtin.stat:
            path: "{{ backup_folder }}/seed-backup-{{ current_time }}.sql"
          register: backup_stat

        - name: Fail if the backup file is empty
          ansible.builtin.fail:
            msg: "Backup file is empty!"
          when: backup_stat.stat.size == 0

        - name: Upload the backup into the S3 bucket
          ansible.builtin.shell: "s3cmd put {{ backup_folder }}/seed-backup-{{ current_time }}.sql s3://skao-bkp-itf/{{ datacenter }}/seed-backup-{{ current_time }}.sql"

        - name: Cleanup seed backups folder
          ansible.builtin.file:
            state: absent
            path: "{{ backup_folder }}"

        - name: Alert that the backup succeeded
          ansible.builtin.uri:
            url: "{{ seed_backup_slack_webhook }}"
            method: POST
            headers:
              Content-Type: application/json
            body:
              text: "Seed backup process succeeded for file seed-backup-{{ current_time }}.sql"
            status_code: 200
            body_format: json
          when:
            - seed_backup_slack_webhook is defined
            - seed_backup_slack_webhook | length > 0
      rescue:
        - name: Alert that the backup failed
          ansible.builtin.uri:
            url: "{{ seed_backup_slack_webhook }}"
            method: POST
            headers:
              Content-Type: application/json
            body:
              text: "@channel Seed backup process failed for file seed-backup-{{ current_time }}.sql"
            status_code: 200
            body_format: json
          when:
            - seed_backup_slack_webhook is defined
            - seed_backup_slack_webhook | length > 0

- name: Backup the Seed VM
  gather_facts: true
  hosts: localhost
  tags:
    - seed-vm-backup
    - never
  vars:
    target_hosts: localhost
    datacenter: low
    backup_folder: /tmp/seed-backups
  tasks:
    - name: Save current timestamp on a variable
      ansible.builtin.set_fact:
        current_time: "{{ ansible_date_time.epoch }}"

    - name: Create seed backups folder if it doesn't exist
      ansible.builtin.file:
        path: "{{ backup_folder }}"
        state: directory
        mode: "0755"

    - name: Backup the seed vm definition file
      become: true
      ansible.builtin.shell: virsh dumpxml seed > "{{ backup_folder }}"/seed-{{ current_time }}.xml

    - name: Backup the seed volume definition files
      become: true
      ansible.builtin.shell: virsh vol-dumpxml {{ item }} --pool default > "{{ backup_folder }}"/{{ item }}-{{ current_time }}.xml
      loop:
        - seed-configdrive
        - seed-data
        - seed-root

    - name: Backup the seed vm disk files
      become: true
      ansible.builtin.shell: virsh backup-begin seed

    - name: Wait for disk backup to finish
      become: true
      ansible.builtin.shell: virsh domjobinfo seed --completed
      register: domjobinfo
      until: '"Completed" in domjobinfo.stdout'
      retries: 30
      delay: 10
      changed_when: false

    - name: Determine the disk backup timestamp
      become: true
      ansible.builtin.shell: bash -c "ls -1A /var/lib/libvirt/images/seed-*.* | cut -d '.' -f 2 | sort | tail -1"
      register: libvirt_backup_timestamp
      changed_when: false

    - name: Copy seed vm disk files
      become: true
      ansible.builtin.shell: bash -c "cp {{ item.src }} {{ item.dest }}"
      loop:
        - src: /var/lib/libvirt/images/seed-configdrive
          dest: "{{ backup_folder }}/seed-configdrive-{{ current_time }}"
        - src: /var/lib/libvirt/images/seed-data.{{ libvirt_backup_timestamp.stdout }}
          dest: "{{ backup_folder }}/seed-data-{{ current_time }}"
        - src: /var/lib/libvirt/images/seed-root.{{ libvirt_backup_timestamp.stdout }}
          dest: "{{ backup_folder }}/seed-root-{{ current_time }}"

    - name: Fix backup files permissions
      become: true
      ansible.builtin.shell: "chown ubuntu:ubuntu {{ backup_folder }}/*"

    - name: Upload the backup into the S3 bucket
      ansible.builtin.shell: "s3cmd put {{ backup_folder }}/{{ item }} s3://skao-bkp-itf/{{ datacenter }}/vm/{{ item }}"
      loop:
        - seed-configdrive-{{ current_time }}
        - seed-configdrive-{{ current_time }}.xml
        - seed-data-{{ current_time }}
        - seed-data-{{ current_time }}.xml
        - seed-root-{{ current_time }}
        - seed-root-{{ current_time }}.xml
        - seed-{{ current_time }}.xml

    - name: Cleanup libvirt disk backups
      become: true
      ansible.builtin.file: 
        path: "{{ item }}"
        state: absent
      loop:
        - /var/lib/libvirt/images/seed-data.{{ libvirt_backup_timestamp.stdout }}
        - /var/lib/libvirt/images/seed-root.{{ libvirt_backup_timestamp.stdout }}

    - name: Cleanup seed backups folder
      ansible.builtin.file:
        state: absent
        path: "{{ backup_folder }}"

- name: Restore the Seed Ironic Database
  gather_facts: true
  hosts: localhost
  tags:
    - seed-ironic-restore
    - never
  vars:
    target_hosts: localhost
    datacenter: low
    backup_folder: /tmp/seed-backups
  tasks:
    - name: Check if restore_timestamp variable is set
      ansible.builtin.assert:
        that:
          - restore_timestamp is defined
          - restore_timestamp | length > 0
        fail_msg: "The mandatory variable 'restore_timestamp' is not set."

    - name: Set restore_file fact
      ansible.builtin.set_fact:
        restore_file: "seed-backup-{{ restore_timestamp }}.sql"

    - name: Create seed backups folder if it doesn't exist
      ansible.builtin.file:
        path: "{{ backup_folder }}"
        state: directory
        mode: "0755"

    - name: Get the backup file from the S3 bucket
      ansible.builtin.shell: "s3cmd get s3://skao-bkp-itf/{{ datacenter }}/{{ restore_file }} {{ backup_folder }}/{{ restore_file }} --force"

    - name: Copy the backup file into the seed
      ansible.builtin.shell: "scp -o StrictHostKeyChecking=no {{ backup_folder }}/{{ restore_file }} stack@seed:seed-restore.sql"

    - name: Stop the ironic services
      ansible.builtin.shell: |
        source /home/ubuntu/kayobe-env &&
        kayobe seed host command run --command "docker exec bifrost_deploy systemctl stop ironic ironic-inspector"
      args:
        executable: /bin/bash

    - name: Restore the ironic database
      ansible.builtin.shell: |
        source /home/ubuntu/kayobe-env &&
        kayobe seed host command run --command "docker exec -i bifrost_deploy mysql < seed-restore.sql"
      args:
        executable: /bin/bash

    - name: Start the ironic services
      ansible.builtin.shell: |
        source /home/ubuntu/kayobe-env &&
        kayobe seed host command run --command "docker exec bifrost_deploy systemctl start ironic ironic-inspector"
      args:
        executable: /bin/bash

    - name: Cleanup seed backups folder
      ansible.builtin.file:
        state: absent
        path: "{{ backup_folder }}"

- name: Restore the Seed VM
  gather_facts: true
  hosts: localhost
  tags:
    - seed-vm-restore
    - never
  vars:
    target_hosts: localhost
    datacenter: low
    backup_folder: /tmp/seed-backups
  tasks:
    - name: Check if restore_timestamp variable is set
      ansible.builtin.assert:
        that:
          - restore_timestamp is defined
          - restore_timestamp | length > 0
        fail_msg: "The mandatory variable 'restore_timestamp' is not set."

    - name: Create seed backups folder if it doesn't exist
      ansible.builtin.file:
        path: "{{ backup_folder }}"
        state: directory
        mode: "0755"

    - name: Get the vm files from the S3 bucket
      ansible.builtin.shell: "s3cmd get s3://skao-bkp-itf/{{ datacenter }}/vm/{{ item }} {{ backup_folder }}/{{ item }} --force"
      loop:
        - seed-configdrive-{{ restore_timestamp }}
        - seed-configdrive-{{ restore_timestamp }}.xml
        - seed-data-{{ restore_timestamp }}
        - seed-data-{{ restore_timestamp }}.xml
        - seed-root-{{ restore_timestamp }}
        - seed-root-{{ restore_timestamp }}.xml
        - seed-{{ restore_timestamp }}.xml

    - name: Define the VM
      become: true
      ansible.builtin.shell: virsh define "{{ backup_folder }}"/seed-{{ restore_timestamp }}.xml

    - name: Define the VM volumes
      become: true
      ansible.builtin.shell: virsh vol-create default "{{ backup_folder }}"/{{ item }}
      loop:
        - seed-configdrive-{{ restore_timestamp }}.xml
        - seed-data-{{ restore_timestamp }}.xml
        - seed-root-{{ restore_timestamp }}.xml

    - name: Move the disk files
      become: true
      ansible.builtin.shell: bash -c "mv {{ item.src }} {{ item.dest }}"
      loop:
        - src: "{{ backup_folder }}/seed-configdrive-{{ restore_timestamp }}"
          dest: /var/lib/libvirt/images/seed-configdrive
        - src: "{{ backup_folder }}/seed-data-{{ restore_timestamp }}"
          dest: /var/lib/libvirt/images/seed-data
        - src: "{{ backup_folder }}/seed-root-{{ restore_timestamp }}"
          dest: /var/lib/libvirt/images/seed-root

    - name: Start the Seed VM
      become: true
      ansible.builtin.shell: virsh start seed

    - name: Wait for SSH access to the seed VM
      local_action:
        module: wait_for
        host: "{{ groups['seed'][0] }}"
        port: 22
        state: started
        timeout: 360

    - name: Run Seed Service Deploy (this may take several minutes)
      ansible.builtin.shell: >
        source /home/ubuntu/kayobe-env &&
        kayobe seed service deploy > /home/ubuntu/seedservicedeploy.txt
      args:
        executable: /bin/bash

    - name: Cleanup seed backups folder
      ansible.builtin.file:
        state: absent
        path: "{{ backup_folder }}"
