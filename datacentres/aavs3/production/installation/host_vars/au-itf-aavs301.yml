---

#defaults for the eda
timescaledb_namespace: ska-tango-archiver
timescaledb_version: 0.1.2
timescaledb_values: {replicaCount: 1, 'persistentVolumes': {'data': {'size': '47Gi'}, 'wal': {'size': '3Gi'}},'resources': {'limits': {'cpu': '2', 'memory': '2Gi'},'requests': {'cpu': '2', 'memory': '2Gi'}}}

# K8s config
k8s_metallb_addresses: "10.134.254.1-10.134.254.99"
k8s_cluster_api_address: "{{ hostvars[inventory_hostname].ansible_default_ipv4.address }}"
k8s_cluster_hostname: au-itf-aavs301.lowitf.skao.int
k8s_externaldns_lb_ip: ""
k8s_ingress_lb_ip: ""

# minikube_additional_names:
#   - "aavs3"

# minikube_additional_ips:
#   - "127.0.0.1"

prometheus_k8s_api_server_addr: "10.134.0.19"

prometheus_localuser: svc_skadev_ansible
monitoring_localgroup: admin

prometheus_alertmanager_url: http://10.135.6.19:9093
prometheus_url: https://10.135.6.19:9090
slack_channel: alerts-aavs3
slack_channel_user: alerts-aavs3-user

prometheus_k8s_external_dns_entry: k8s.aavs3.skao.int

project_name: "AAVS3"

prometheus_datacentre: aavs3-monitor

prometheus_server_kubectl_version: 'v1.25.5'

prometheus_openstack_exporter_addr: ""

prometheus_snpm_exporter_enabled: true

grafana_datasource_url: https://10.135.6.19:9090
grafana_ceph_enabled: false
grafana_elasticstack_enabled: false
grafana_kubernetes_enabled: true
grafana_gitlab_runners_enabled: false
grafana_cadvisor_enabled: true
grafana_node_exporter_enabled: true
grafana_openstack_exporter_enabled: false
grafana_idrac_exporter_enabled: true
grafana_riello_ups_exporter_enabled: false
grafana_alerts_enabled: true

thanos_query_endpoints: []

prometheus_static_node_metric_relabel_configs:
  - action: replace
    regex: 10\.134\.0\.19:9100
    replacement: au-itf-aavs301:9100
    source_labels:
      - instance
    target_label: instance
  - action: replace
    regex: 10\.134\.0\.19:9323
    replacement: au-itf-aavs301:9323
    source_labels:
      - instance
    target_label: instance
  - action: replace
    regex: 10\.134\.0\.19:10249
    replacement: au-itf-aavs301:10249
    source_labels:
      - instance
    target_label: instance

prometheus_scrape_configs:
  - job_name: "kube-proxy"
    static_configs:
      - targets:
        - "10.134.0.19:10249"
    metric_relabel_configs: "{{ prometheus_static_node_metric_relabel_configs +
      prometheus_node_metric_relabel_configs }}"
    relabel_configs:
      - source_labels: ['__name__']
        regex: '.*'
        action: replace
        target_label: 'datacentre'
        replacement: "{{ prometheus_datacentre }}"

  - job_name: "kube-state-metrics"
    static_configs:
      - targets:
        - "10.134.0.19:32080"  # load balancer
    relabel_configs:
      - source_labels: ['__name__']
        regex: '.*'
        action: replace
        target_label: 'datacentre'
        replacement: "{{ prometheus_datacentre }}"

  - job_name: "k8stelemetry"
    static_configs:
      - targets:
        - "10.134.0.19:32081"  # load balancer
    relabel_configs:
      - source_labels: ['__name__']
        regex: '.*'
        action: replace
        target_label: 'datacentre'
        replacement: "{{ prometheus_datacentre }}"

  - job_name: "prometheus"
    scheme: https
    tls_config:
      insecure_skip_verify: true
    static_configs:
      - targets:
          - "{{ ansible_fqdn | default(ansible_host) | default('localhost') }}:9090"

  - job_name: "node"
    static_configs:
      - targets:
        - "10.134.0.19:9100"
    metric_relabel_configs: "{{ prometheus_static_node_metric_relabel_configs +
       prometheus_node_metric_relabel_configs }}"
    relabel_configs:
      - source_labels: ['__name__']
        regex: '.*'
        action: replace
        target_label: 'datacentre'
        replacement: "{{ prometheus_datacentre }}"

  - job_name: "docker"
    static_configs:
      - targets:
        - "10.134.0.19:9323"
    relabel_configs:
      - source_labels: ['__name__']
        regex: '.*'
        action: replace
        target_label: 'datacentre'
        replacement: "{{ prometheus_datacentre }}"

  - job_name: snmp
    scrape_interval: 60s
    scrape_timeout: 60s
    static_configs:
      - targets:
        - 10.135.0.19
    metrics_path: /snmp
    params:
      module: [dell_idrac]
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: 10.135.6.19:9116

  - job_name: postgres_exporter
    scrape_interval: 30s
    scrape_timeout: 30s
    static_configs:
    - targets:
      - 10.135.6.19:9187
    metrics_path: /metrics

  - job_name: monitoring
    scrape_interval: 5s
    static_configs:
      - targets:
        - 10.135.6.19:9080
    relabel_configs:
      - source_labels: ['__name__']
        regex: '.*'
        action: replace
        target_label: 'datacentre'
        replacement: "{{ prometheus_datacentre }}"

# Binderhub
k8s_external_dns_entry: 10.134.0.19
k8s_binderhub_singleuser: false # goes with transport http and no oauth integration
k8s_binderhub_image:
  name: artefact.skao.int/binderhub
  tag: 0.2.0-ska-0.1.0
k8s_binderhub_transport: http # only using IP address currently  - see k8s_external_dns_entry
k8s_binderhub_oci_registry_host: "10.135.6.19:5000" # pointing to private nexus instance
k8s_binderhub_oci_registry_host_full_url: "http://{{k8s_binderhub_oci_registry_host}}"
k8s_binderhub_force_ssl_redirect: true
k8s_binderhub_azuread_enabled: false
k8s_binderhub_azuread_client_id: "{{ lookup('ansible.builtin.env', 'K8S_BINDERHUB_AZUREAD_CLIENT_ID', default=secrets['k8s_binderhub_azuread_client_id']) }}"
k8s_binderhub_azuread_client_secret: "{{ lookup('ansible.builtin.env', 'K8S_BINDERHUB_AZUREAD_CLIENT_SECRET', default=secrets['k8s_binderhub_azuread_client_secret']) }}"
k8s_binderhub_azuread_tenant_id: "{{ lookup('ansible.builtin.env', 'K8S_BINDERHUB_AZUREAD_TENANT_ID', default=secrets['k8s_binderhub_azuread_tenant_id']) }}"
k8s_binderhub_gitlab_client_id: "{{ lookup('ansible.builtin.env', 'K8S_BINDERHUB_GITLAB_CLIENT_ID', default=secrets['k8s_binderhub_gitlab_client_id']) }}"
k8s_binderhub_gitlab_client_secret: "{{ lookup('ansible.builtin.env', 'K8S_BINDERHUB_GITLAB_CLIENT_SECRET', default=secrets['k8s_binderhub_gitlab_client_secret']) }}"


### Server Resources & Configuration

## Grafana
grafana_server_cpus: 0.3
grafana_server_memory: 256M
grafana_server_memory_swap: unlimited
grafana_server_memory_swappiness: 60
# # Fine-tuning
# grafana_quota_enabled: true
# grafana_quota_global_session: 1

## Thanos
thanos_sidecar_server_cpus: 0.1
thanos_sidecar_server_memory: 32M
thanos_sidecar_server_memory_swap: unlimited
thanos_sidecar_server_memory_swappiness: 60

## Prometheus
prometheus_server_cpus: 0.3
prometheus_server_memory: 512M
prometheus_server_memory_swap: unlimited
prometheus_server_memory_swappiness: 60
# Fine-tuning
prometheus_server_swapfile_fallocate: false  # AAVS3 nodes already have swap

## Alertmanager
alertmanager_server_cpus: 0.1
alertmanager_server_memory: 32M
alertmanager_server_memory_swap: unlimited
alertmanager_server_memory_swappiness: 60

## Filebeat
logging_filebeat_server_cpus: 0.1
logging_filebeat_server_memory: 80M
logging_filebeat_server_memory_swap: unlimited
logging_filebeat_server_memory_swappiness: 60
# Fine-tuning
logging_filebeat_queue_disk_enabled: true
logging_filebeat_queue_disk_max_size: 10GB

## SNMP
snmp_server_cpus: 0.1
snmp_server_memory: 16M
snmp_server_memory_swap: unlimited
snmp_server_memory_swappiness: 60

## Postgres
postgres_exporter_hostname: 10.134.254.3
postgres_exporter_version_cpu: 0.1
postgres_exporter_version_memory: 16M
postgres_exporter_version_swap: unlimited
postgres_exporter_version_swapiness: 60