---

###############################################################################
# Infrastructure VM configuration.

# List of network interfaces to attach to the infra VM.
#infra_vm_interfaces:
# Network interfaces that the VM is attached to.
infra_vm_network_interfaces:
  - bm_cloud
  - bm_external
  - bm_storage

# Mandatory: All networks must have an interface defined.
bm_cloud_interface: ens2
bm_external_interface: ens3
bm_storage_interface: ens4

# Route via the vm to the outside world.
bm_external_gateway: 10.134.0.1

# By default kayobe will connect to a host via admin_oc_net.
# As we have not attached this VM to this network, we must override
# ansible_host.
ansible_host: "10.135.6.41"
hypervisor_host: "au-itf-cloud01"
###############################################################################
# Infrastructure VM configuration.

# Name of the infra VM.
infra_vm_name: "au-itf-k8s-master02"

# Memory in MB.
infra_vm_memory_mb: "{{ 8 * 1024 }}"

# Number of vCPUs.
infra_vm_vcpus: 4

# List of volumes.
infra_vm_volumes:
  - "{{ infra_vm_root_volume }}"
  # - "{{ infra_vm_data_volume }}"

# Root volume.
infra_vm_root_volume:
  name: "{{ infra_vm_name }}-root"
  pool: "{{ infra_vm_pool }}"
  capacity: "{{ infra_vm_root_capacity }}"
  format: "{{ infra_vm_root_format }}"
  image: "{{ infra_vm_root_image }}"

# Data volume.
infra_vm_data_volume:
  name: "{{ infra_vm_name }}-data"
  pool: "{{ infra_vm_pool }}"
  capacity: "{{ infra_vm_data_capacity }}"
  format: "{{ infra_vm_data_format }}"

# Name of the storage pool for the infra VM volumes.
infra_vm_pool: default

# manually built pool from here:
# https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/virtualization_administration_guide/sect-virtualization-storage_pools-creating-local_directories-virsh


# Capacity of the infra VM root volume.
infra_vm_root_capacity: 50G

# Format of the infra VM root volume.
infra_vm_root_format: qcow2

# Base image for the infra VM root volume. Default is
# "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img"
# when os_distribution is "ubuntu", or
# https://dl.rockylinux.org/pub/rocky/8/images/Rocky-8-GenericCloud.latest.x86_64.qcow2
# when os_distribution is "rocky",
# or
# "https://cloud.centos.org/centos/8-stream/x86_64/images/CentOS-Stream-GenericCloud-8-20220913.0.x86_64.qcow2"
# otherwise.
infra_vm_root_image: >-
  {%- if os_distribution == 'ubuntu' %}
  https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img
  {%- elif os_distribution == 'rocky' %}
  https://dl.rockylinux.org/pub/rocky/8/images/Rocky-8-GenericCloud.latest.x86_64.qcow2
  {%- else -%}
  https://cloud.centos.org/centos/8-stream/x86_64/images/CentOS-Stream-GenericCloud-8-20220913.0.x86_64.qcow2
  {%- endif %}

# Capacity of the infra VM data volume.
infra_vm_data_capacity: 500G

# Format of the infra VM data volume.
infra_vm_data_format: qcow2

# List of network interfaces to attach to the infra VM.
infra_vm_interfaces: "{{ network_interfaces | sort | map('net_libvirt_vm_network') | list }}"

# Hypervisor that the VM runs on.
infra_vm_hypervisor: "{{ hypervisor_host }}"

# Customise ansible_ssh_extra_args for the test that checks SSH connectivity
# after provisioning. Defaults to disabling ssh host key checking.
infra_vm_wait_connection_ssh_extra_args: '-o StrictHostKeyChecking=no'

# OS family. Needed for config drive generation.
infra_vm_os_family: "{{ 'RedHat' if os_distribution == 'centos' else 'Debian' }}"

###############################################################################
# Infrastructure VM node configuration.

# User with which to access the infrastructure vm via SSH during bootstrap, in
# order to setup the Kayobe user account. Default is {{ os_distribution }}.
infra_vm_bootstrap_user: "{{ os_distribution }}"

###############################################################################
# Infrastructure VM network interface configuration.


# List of networks to which infrastructure vm nodes are attached.
# infra_vm_network_interfaces: []

# List of networks to which infrastructure vm nodes are attached.
# infra_vm_network_interfaces: >
#   {{ (infra_vm_default_network_interfaces +
#       infra_vm_extra_network_interfaces) | select | unique | list }}

# List of default networks to which infrastructure vm nodes are attached.
infra_vm_default_network_interfaces: >
  {{ [admin_oc_net_name] | select | unique | list }}

# List of extra networks to which infrastructure vm nodes are attached.
infra_vm_extra_network_interfaces: []

###############################################################################
# Infrastructure VM node software RAID configuration.

# List of software RAID arrays. See mrlesmithjr.mdadm role for format.
infra_vm_mdadm_arrays: []

###############################################################################
# Infrastructure VM node encryption configuration.

# List of block devices to encrypt. See stackhpc.luks role for format.
infra_vm_luks_devices: []

###############################################################################
# Infrastructure VM node LVM configuration.

# List of infrastructure vm volume groups. See mrlesmithjr.manage-lvm role for
# format.
infra_vm_lvm_groups: "{{ infra_vm_lvm_groups_default + infra_vm_lvm_groups_extra }}"

# Default list of infrastructure vm volume groups. See mrlesmithjr.manage-lvm
# role for format.
infra_vm_lvm_groups_default: "{{ [infra_vm_lvm_group_data] if infra_vm_lvm_group_data_enabled | bool else [] }}"

# Additional list of infrastructure vm volume groups. See mrlesmithjr.manage-lvm
# role for format.
infra_vm_lvm_groups_extra: []

# Whether a 'data' LVM volume group should exist on the infrastructure vm. By
# default this contains a 'docker-volumes' logical volume for Docker volume
# storage. It will also be used for Docker container and image storage if
# 'docker_storage_driver' is set to 'devicemapper'. Default is true if
# 'docker_storage_driver' is set to 'devicemapper', or false otherwise.
infra_vm_lvm_group_data_enabled: "{{ docker_storage_driver == 'devicemapper' }}"

# Infrastructure VM LVM volume group for data. See mrlesmithjr.manage-lvm role
# for format.
infra_vm_lvm_group_data:
  vgname: data
  disks: "{{ infra_vm_lvm_group_data_disks }}"
  create: True
  lvnames: "{{ infra_vm_lvm_group_data_lvs }}"

# List of disks for use by infrastructure vm LVM data volume group. Default to
# an invalid value to require configuration.
infra_vm_lvm_group_data_disks:
  - changeme

# List of LVM logical volumes for the data volume group.
infra_vm_lvm_group_data_lvs:
  - "{{ infra_vm_lvm_group_data_lv_docker_volumes }}"

# Docker volumes LVM backing volume.
infra_vm_lvm_group_data_lv_docker_volumes:
  lvname: docker-volumes
  size: "{{ infra_vm_lvm_group_data_lv_docker_volumes_size }}"
  create: True
  filesystem: "{{ infra_vm_lvm_group_data_lv_docker_volumes_fs }}"
  mount: True
  mntp: /var/lib/docker/volumes

# Size of docker volumes LVM backing volume.
infra_vm_lvm_group_data_lv_docker_volumes_size: 75%VG

# Filesystem for docker volumes LVM backing volume. ext4 allows for shrinking.
infra_vm_lvm_group_data_lv_docker_volumes_fs: ext4

###############################################################################
# Infrastructure VM node sysctl configuration.

# Dict of sysctl parameters to set.
infra_vm_sysctl_parameters: {}

###############################################################################
# Infrastructure VM node tuned configuration.

# Builtin tuned profile to use. Format is same as that used by giovtorres.tuned
# role. Default is virtual-guest.
infra_vm_tuned_active_builtin_profile: "virtual-guest"

###############################################################################
# Infrastructure VM node user configuration.

# List of users to create. This should be in a format accepted by the
# singleplatform-eng.users role.
infra_vm_users: "{{ users_default }}"

###############################################################################
# Infrastructure VM node firewalld configuration.

# Whether to install and enable firewalld.
infra_vm_firewalld_enabled: false

# A list of zones to create. Each item is a dict containing a 'zone' item.
infra_vm_firewalld_zones: []

# A firewalld zone to set as the default. Default is unset, in which case the
# default zone will not be changed.
infra_vm_firewalld_default_zone:

# A list of firewall rules to apply. Each item is a dict containing arguments
# to pass to the firewalld module. Arguments are omitted if not provided, with
# the following exceptions:
# - offline: true
# - permanent: true
# - state: enabled
infra_vm_firewalld_rules: []


###############################################################################
# Dummy variable to allow Ansible to accept this file.
workaround_ansible_issue_8743: yes
