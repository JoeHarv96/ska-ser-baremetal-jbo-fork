---
###############################################################################
# General configuration of inspection.

# Timeout of hardware inspection on baremetal compute nodes, in seconds.
# Default is 1200.
#inspector_inspection_timeout:

###############################################################################
# Ironic inspector PXE configuration.

# List of extra kernel parameters for the inspector default PXE configuration.
#inspector_extra_kernel_options:

# URL of Ironic Python Agent (IPA) kernel image.
#inspector_ipa_kernel_upstream_url:

# URL of checksum of Ironic Python Agent (IPA) kernel image.
#inspector_ipa_kernel_checksum_url:

# Algorithm of checksum of Ironic Python Agent (IPA) kernel image.
#inspector_ipa_kernel_checksum_algorithm:

# URL of Ironic Python Agent (IPA) ramdisk image.
#inspector_ipa_ramdisk_upstream_url:

# URL of checksum of Ironic Python Agent (IPA) ramdisk image.
#inspector_ipa_ramdisk_checksum_url:

# Algorithm of checksum of Ironic Python Agent (IPA) ramdisk image.
#inspector_ipa_ramdisk_checksum_algorithm:

###############################################################################
# Ironic inspector processing configuration.

# List of of default inspector processing plugins.
#inspector_processing_hooks_default:

# List of of additional inspector processing plugins.
#inspector_processing_hooks_extra:

# List of of additional inspector processing plugins.
#inspector_processing_hooks:

# Which MAC addresses to add as ports during introspection. One of 'all',
# 'active' or 'pxe'.
#inspector_add_ports:

# Which ports to keep after introspection. One of 'all', 'present', or 'added'.
#inspector_keep_ports:

# Whether to enable discovery of nodes not managed by Ironic.
#inspector_enable_discovery:

# The Ironic driver with which to register newly discovered nodes.
#inspector_discovery_enroll_node_driver:

###############################################################################
# Ironic inspector configuration.

# Ironic inspector IPMI username to set.
#inspector_ipmi_username:

# Ironic inspector IPMI password to set.
#inspector_ipmi_password:

# Ironic inspector default network interface name on which to check for an LLDP
# switch port description to use as the node's name.
#inspector_lldp_switch_port_interface_default:

# Ironic inspector map from hostname to network interface name on which to
# check for an LLDP switch port description to use as the node's name.
#inspector_lldp_switch_port_interface_map:

###############################################################################
# Ironic inspector introspection rules configuration.

# Ironic inspector rule to set IPMI credentials.
#inspector_rule_ipmi_credentials:

# Ironic inspector rule to set deployment kernel.
#inspector_rule_deploy_kernel:

# Ironic inspector rule to set deployment ramdisk.
#inspector_rule_deploy_ramdisk:

# Ironic inspector rule to initialise root device hints.
#inspector_rule_root_hint_init:

# Ironic inspector rule to set serial root device hint.
#inspector_rule_root_hint_serial:

# Ironic inspector rule to set the interface on which the node PXE booted.
#inspector_rule_set_pxe_interface_mac:

# Ironic inspector rule to set the node's name from an interface's LLDP switch
# port description.
#inspector_rule_lldp_switch_port_desc_to_name:

# Ironic inspector rule to save introspection data to the node.
#inspector_rule_save_data:

# List of default ironic inspector rules.
#inspector_rules_default:


# Generic PowerEdge R7525 rule
poweredge_r7525:
  description: "PowerEdge R7525 generic rule"
  conditions:
    - field: "data://inventory.system_vendor.product_name"
      op: "contains"
      value: "PowerEdge R7525"
  actions:
    - action: "set-attribute"
      path: "driver"
      value: "idrac"
    - action: "set-attribute"
      path: "bios_interface"
      value: "idrac-redfish"
    - action: "set-attribute"
      path: "inspect_interface"
      value: "inspector"
    - action: "set-attribute"
      path: "management_interface"
      value: "idrac-redfish"
    - action: "set-attribute"
      path: "power_interface"
      value: "idrac-redfish"
    - action: "set-attribute"
      path: "raid_interface"
      value: "idrac-redfish"
    - action: "set-attribute"
      path: "driver_info/redfish_username"
      value: "root"
    - action: "set-attribute"
      path: "driver_info/redfish_verify_ca"
      value: False
    - action: "set-attribute"
      path: "driver_info/redfish_system_id"
      value: "/redfish/v1/Systems/System.Embedded.1"
    - action: "set-attribute"
      path: "instance_info/image_source"
      value: "http://10.135.6.5:8080/ska-ubuntu-jammy.qcow2"
    - action: "set-attribute"
      path: "instance_info/image_checksum"
      value: "http://10.135.6.5:8080/ska-ubuntu-jammy.qcow2.CHECKSUMS"
    - action: "set-attribute"
      path: "properties/capabilities"
      value: "cpu_vt:true,cpu_aes:true,cpu_hugepages:true,cpu_hugepages_1g:true,cpu_txt:true,boot_option:local,secure_boot:true"

# Specific rule for cloud01
au_itf_cloud01:
  description: "au-itf-cloud01 rule"
  conditions:
    - field: "data://inventory.system_vendor.serial_number"
      op: "eq"
      value: "1X6YPT3"
  actions:
    - action: "set-attribute"
      path: "name"
      value: "au-itf-cloud01"
    - action: "set-attribute"
      path: "driver_info/redfish_address"
      value: "https://10.135.0.10"


# Specific rule for cloud02
au_itf_cloud02:
  description: "au-itf-cloud02 rule"
  conditions:
    - field: "data://inventory.system_vendor.serial_number"
      op: "eq"
      value: "JW6YPT3"
  actions:
    - action: "set-attribute"
      path: "name"
      value: "au-itf-cloud02"
    - action: "set-attribute"
      path: "driver_info/redfish_address"
      value: "https://10.135.0.11"

# Specific rule for cloud03
au_itf_cloud03:
  description: "au-itf-cloud03 rule"
  conditions:
    - field: "data://inventory.system_vendor.serial_number"
      op: "eq"
      value: "1WCRXT3"
  actions:
    - action: "set-attribute"
      path: "name"
      value: "au-itf-cloud03"
    - action: "set-attribute"
      path: "driver_info/redfish_address"
      value: "https://10.135.0.12"

# Specific rule for cloud04
au_itf_cloud04:
  description: "au-itf-cloud04 rule"
  conditions:
    - field: "data://inventory.system_vendor.serial_number"
      op: "eq"
      value: "2WCRXT3"
  actions:
    - action: "set-attribute"
      path: "name"
      value: "au-itf-cloud04"
    - action: "set-attribute"
      path: "driver_info/redfish_address"
      value: "https://10.135.0.13"

# Specific rule for cloud05
au_itf_cloud05:
  description: "au-itf-cloud05 rule"
  conditions:
    - field: "data://inventory.system_vendor.serial_number"
      op: "eq"
      value: "3WCRXT3"
  actions:
    - action: "set-attribute"
      path: "name"
      value: "au-itf-cloud05"
    - action: "set-attribute"
      path: "driver_info/redfish_address"
      value: "https://10.135.0.14"

# Specific rule for ceph01
au_itf_ceph01:
  description: "au-itf-ceph01 rule"
  conditions:
    - field: "data://inventory.system_vendor.serial_number"
      op: "eq"
      value: "GVCRXT3"
  actions:
    - action: "set-attribute"
      path: "name"
      value: "au-itf-ceph01"
    - action: "set-attribute"
      path: "driver_info/redfish_address"
      value: "https://10.135.0.15"

# Specific rule for ceph02
au_itf_ceph02:
  description: "au-itf-ceph02 rule"
  conditions:
    - field: "data://inventory.system_vendor.serial_number"
      op: "eq"
      value: "HVCRXT3"
  actions:
    - action: "set-attribute"
      path: "name"
      value: "au-itf-ceph02"
    - action: "set-attribute"
      path: "driver_info/redfish_address"
      value: "https://10.135.0.16"

# Specific rule for ceph03
au_itf_ceph03:
  description: "au-itf-ceph03 rule"
  conditions:
    - field: "data://inventory.system_vendor.serial_number"
      op: "eq"
      value: "JVCRXT3"
  actions:
    - action: "set-attribute"
      path: "name"
      value: "au-itf-ceph03"
    - action: "set-attribute"
      path: "driver_info/redfish_address"
      value: "https://10.135.0.17"

# Specific rule for au-itf-aavs301
au_itf_aavs301:
  description: "au-itf-aavs301 rule"
  conditions:
    - field: "data://inventory.system_vendor.serial_number"
      op: "eq"
      value: "2X6YPT3"
  actions:
    - action: "set-attribute"
      path: "name"
      value: "au-itf-aavs301"
    - action: "set-attribute"
      path: "driver_info/redfish_address"
      value: "https://10.135.0.19"

# Specific rule for au-itf-aavs302
au_itf_aavs302:
  description: "au-itf-aavs302 rule"
  conditions:
    - field: "data://inventory.system_vendor.serial_number"
      op: "eq"
      value: "3X6YPT3"
  actions:
    - action: "set-attribute"
      path: "name"
      value: "au-itf-aavs302"
    - action: "set-attribute"
      path: "driver_info/redfish_address"
      value: "https://10.135.0.20"

# List of additional ironic inspector rules.
inspector_rules_extra:
  - "{{ poweredge_r7525 }}"
  - "{{ au_itf_cloud01 }}"
  - "{{ au_itf_cloud02 }}"
  - "{{ au_itf_cloud03 }}"
  - "{{ au_itf_cloud04 }}"
  - "{{ au_itf_cloud05 }}"
  - "{{ au_itf_ceph01 }}"
  - "{{ au_itf_ceph02 }}"
  - "{{ au_itf_ceph03 }}"
  - "{{ au_itf_aavs301 }}"
  - "{{ au_itf_aavs302 }}"


# List of all ironic inspector rules.
#inspector_rules:

###############################################################################
# Dell switch LLDP workaround configuration.

# Some Dell switch OSs (including Dell Network OS 9.10(0.1)) do not support
# sending interface port description TLVs correctly. Instead of sending the
# interface description, they send the interface name (e.g. TenGigabitEthernet
# 1/1/1). This breaks the discovery process which relies on Ironic node
# introspection data containing the node's name in the interface port
# description. We work around this here by creating an introspection rule for
# each ironic node that matches against the switch system and the relevant
# interface name, then sets the node's name appropriately.

# Ansible group containing switch hosts to which the workaround should be
# applied.
#inspector_dell_switch_lldp_workaround_group:

###############################################################################
# Inspection store configuration.
# The inspection store provides a Swift-like service for storing inspection
# data which may be useful in environments without Swift.

# Whether the inspection data store is enabled.
#inspector_store_enabled:

# Port on which the inspection data store should listen.
#inspector_store_port:

###############################################################################
# Dummy variable to allow Ansible to accept this file.
workaround_ansible_issue_8743: yes
